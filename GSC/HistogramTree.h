/**
 * @file   HistogramTree.h
 * @author Jeff Dandoy <jeff.dandoy@cern.ch>
 *
 */

#ifndef GSGC_HistogramTree_H
#define GSGC_HistogramTree_H

// ROOT include(s):
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"

#include <vector>

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"
#include <xAODAnaHelpers/HelperFunctions.h>

class HistogramTree : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

    /** @brief If JZW (Pythia8), reweight to total number of events, not sum of event weights */
    bool m_isJZW = true;

    /** @brief Input file name that contains GSC corrections */
    std::string m_GSCFile = "FileName";
    /** @brief Sequentialy GSC stages to apply */
    std::string m_GSCOrder = "Tile0";
    /** @brief jet type */
    std::string m_jetType = "AntiKt4EMTopo";

    /** @brief Comma-separated string of mu ranges for additional response histograms */
    std::string m_muRanges = "";
    /** @brief Comma-separated string of npv ranges for additional response histograms */
    std::string m_npvRanges = "";

    /** @brief Eta binning of GSC */
    std::string m_etaBins = "Nominal";
    /** @brief Pt binning of GSC */
    std::string m_ptBins = "20,25,30,40,50,60,80,100,120,140,160,180,200,250,300,350,400,500,600,800,1000,1200,1500,1800,2100,2500,3500,4500,7000";
    /** @brief Coarse eta binning of Nseg GSC */
    std::string m_etaBins_coarse = "0,13,19,27";
    /** @brief Energy binning of Nseg GSC */
    std::string m_EBins = "0,10,20,30,40,50,60,80,100,150,200,400,600,800,1000,1200,1600,2000,3000,4500";

    /** @brief Max eta for Tile0 */
    float m_maxEta_Tile0 = 1.7;
    /** @brief Max eta for EM3 */
    float m_maxEta_EM3 = 3.5;
    /** @brief Max eta for Ntrk */
    float m_maxEta_Ntrk = 2.5;
    /** @brief Max eta for Wtrk */
    float m_maxEta_Wtrk = 2.5;
    /** @brief Max eta for CF */
    float m_maxEta_CF = 2.7;
    /** @brief Max eta for Nseg (0,1.3,1.9.2.7) */
    float m_maxEta_Nseg = 2.7;

    /** @brief Apply isolation decision from TTree branch */
    bool m_applyIsolation = true;

    /** @brief Enum of know GSC stages */
    enum GSCType {NoGSCType, Tile0, EM3, Ntrk, Wtrk, CF, Nseg};

  private:


    /** @brief Event counter */
    int m_eventCounter;     //!
    /** @brief Initial number of events in the AOD for weighting purposes.  Taken from metadta histogram */
    float m_totalNumEvents;  //!

    /** @brief Sequential vector of GSC enums to apply */
    std::vector<GSCType> m_GSCEnums;
    /** @brief Sequential map of GSC enums to their eta ranges */
    std::map< GSCType, std::vector< std::pair<float, float> > > m_GSCEtaEdges;
    /** @brief Sequential map of GSC enums to their vector of histograms*/
    std::map< GSCType, std::vector< TH2F* > > m_GSCHists_eta;
    /** @brief Sequential map of GSC enums to a 3D histogram.  OBSOLETE because 3D interpolation does not work*/
    //std::map< GSCType, TH3F* > m_GSCHists;

    /** @brief Vector of all eta edges*/
    std::vector<float> m_etaEdges; //!
    /** @brief Vector of all coarse eta edges*/
    std::vector<float> m_etaEdges_coarse; //!
    /** @brief Vector of all pt edges*/
    std::vector<float> m_ptEdges; //!
    /** @brief Vector of all energy edges*/
    std::vector<float> m_energyEdges; //!


    /** @brief Vector of mu edges*/
    std::vector<int> m_muEdges; //!
    /** @brief Vector of npv edges*/
    std::vector<int> m_npvEdges; //!

    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:

    // this is a standard constructor
    HistogramTree (std::string className = "HistogramTree");

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();

    /** @brief Function to calculation the deltaR between two physics objects */
    double DeltaR(double eta1, double phi1, double eta2, double phi2);

    /** @brief Apply the GSC corrections for this jet */
    EL::StatusCode applyGSC(xAOD::JetFourMom_t *calibJet, float detEta, float thisTile0, float thisEM3, float thisNtrk, float thisWtrk, float thisCF, float thisNseg);
    /** @brief Load the GSC calibration histograms */
    EL::StatusCode loadGSC();
    /** @brief Function to split string by separator */
    std::vector< std::string > splitLine( std::string inLine, char sep=',' );

    /// @cond
    // this is needed to distribute the algorithm to the workers
    ClassDef(HistogramTree, 1);
    /// @endcond

  private:

    /** @brief TTree branch names to be connected */
    int      runNumber;    //!
    Long64_t eventNumber;  //!
    int mcChannelNumber; //!
    int NPV;    //!
    float averageInteractionsPerCrossing;    //!
    float correct_mu; //!
    float weight;   //!
    float weight_pileup;    //!
    float weight_mcEventWeight;    //!
    float weight_xs; //!
    float weight_prescale; //!

    int njet; //!
    float rho; //!


    std::vector<float> *jet_pt; //!
    std::vector<float> *jet_eta; //!
    std::vector<float> *jet_phi; //!
    std::vector<float> *jet_E; //!
    std::vector<float> *jet_DetEta; //!
    std::vector<float> *jet_Jvt; //!
    std::vector<float> *jet_PartonTruthLabelID; //!
    std::vector<float> *jet_true_pt; //!
    std::vector<float> *jet_true_eta; //!
    std::vector<float> *jet_true_phi; //!
    std::vector<float> *jet_true_e; //!
    std::vector<float> *jet_respE; //!
    std::vector<float> *jet_respPt; //!
    std::vector<float> *jet_ConstitE; //!

    std::vector<float> *jet_Wtrk1000; //!
    std::vector<int> *jet_Ntrk1000; //!
    std::vector<int> *jet_nMuSeg; //!
    std::vector<float> *jet_ChargedFraction; //!
    std::vector< std::vector<float> > *jet_EnergyPerSampling; //!
    std::vector<int> *jet_iso_halfPt; //!

    /** @brief Vectors where histograms will be stored */

    std::vector< std::vector< TH2F* > > h_response_vs_Tile0; //!
    std::vector< std::vector< TH2F* > > h_response_vs_EM3; //!
    std::vector< std::vector< TH2F* > > h_response_vs_Wtrk; //!
    std::vector< std::vector< TH2F* > > h_response_vs_Ntrk; //!
    std::vector< std::vector< TH2F* > > h_response_vs_Nseg; //!
    std::vector< std::vector< TH2F* > > h_response_vs_CF; //!
    std::vector< TH2F* > h_response_vs_pt_coarse; //!
    std::vector< TH2F* > h_response_vs_E_coarse; //!
    std::vector< TH2F* > h_response_vs_pt; //!
    std::vector< TH2F* > h_response_vs_E; //!
    std::vector< TH2F* > h_LQ_response_vs_pt; //!
    std::vector< TH2F* > h_LQ_response_vs_E; //!
    std::vector< TH2F* > h_gluon_response_vs_pt; //!
    std::vector< TH2F* > h_gluon_response_vs_E; //!
    std::vector< TH2F* > h_b_response_vs_pt; //!
    std::vector< TH2F* > h_b_response_vs_E; //!
    std::vector< TH2F* > h_c_response_vs_pt; //!
    std::vector< TH2F* > h_c_response_vs_E; //!
    std::vector< TProfile* > h_centerPt; //!
    std::vector< TProfile* > h_centerE; //!
    std::vector< TProfile* > h_centerPt_coarse; //!
    std::vector< TProfile* > h_centerE_coarse; //!

    std::vector< std::vector< TH2F* > > h_response_vs_mu_pt; //!
    std::vector< std::vector< TH2F* > > h_response_vs_mu_E; //!
    std::vector< std::vector< TH2F* > > h_response_vs_npv_pt; //!
    std::vector< std::vector< TH2F* > > h_response_vs_npv_E; //!


    TH1F* h_NPV;    //!
    TH1F* h_averageInteractionsPerCrossing;    //!
    TH1F* h_correct_mu;    //!
    TH1F* h_njet;    //!

    //* @brief Number of leading jets to make kinematic histograms for */
    unsigned int numHistJets; //!

    std::vector<TH1F*> vh_jet_pt; //!
    std::vector<TH1F*> vh_jet_eta; //!
    std::vector<TH1F*> vh_jet_phi; //!
    std::vector<TH1F*> vh_jet_E; //!
    std::vector<TH1F*> vh_jet_DetEta; //!
    std::vector<TH1F*> vh_jet_Jvt; //!
    std::vector<TH1F*> vh_jet_PartonTruthLabelID; //!

    TH1F* h_jet_pt_all; //!
    TH1F* h_jet_eta_all; //!
    TH1F* h_jet_phi_all; //!
    TH1F* h_jet_E_all; //!


};

#endif
