import ROOT
from xAH_config import xAH_config

c = xAH_config()

#jetCont = "EMTopoLowPt"
#jetCont = "EMTopo"
jetCont = "LCTopo"
#jetCont = "LCTopoLowPt"
#jetCont = "EMPFlow"

#calibSeq = "JetArea_Residual_EtaJES"
calibSeq = "JetArea_Residual_EtaJES_GSC"

#EM pre-recommendations
#calibConfig = "JES_MC16Recommendation_Aug2017.config"
#EM & LC final recommendations
#calibConfig = "JES_MC16Recommendation_Oct2017_GSCLOCAL.config"
#PF final recommendation
#calibConfig = "JES_MC16Recommendation_PFlow_Oct2017_GSCLOCAL.config"

calibConfig = "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config"
#calibConfig = "JES_data2017_2016_2015_Recommendation_PFlow_Feb2018_rel21.config"

#trigger = "HLT_j380"
trigger = ""

GRLs = ["GSC/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
        "GSC/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
        "GSC/data17_13TeV.periodAllYear_DetStatus-v94-pro21-07_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
        ]
iLumis = ["GSC/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
          "GSC/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root",
          "GSC/ilumicalc_histograms_None_325713-334779_OflLumi-13TeV-001.root"
          ]

GRLlist = ','.join(GRLs)
Lumilist = ','.join(iLumis)


c.setalg("BasicEventSelection",    { "m_name"   : "BasicSelection",
                                     "m_msgLevel"      : "info",
                                     "m_applyGRLCut"                 : True,
                                     "m_GRLxml"                      : GRLlist,
                                     "m_derivationName"              : "JETM1Kernel",
                                     "m_useMetaData"                 : True,
                                     "m_storePassHLT"                : True,
                                     "m_storeTrigDecisions"          : True,
                                     #"m_applyTriggerCut"             : True, 
                                     "m_applyTriggerCut"             : False, 
                                     "m_triggerSelection"            : trigger,
                                     "m_checkDuplicatesData"         : False,
                                     "m_applyEventCleaningCut"       : True,
                                     "m_applyPrimaryVertexCut"       : True,
                                     "m_doPUreweighting"       : False,
                                     "m_PRWFileNames"          : "InsituBalance/mc15c_v2_defaults.NotRecommended.prw.root",
                                     "m_lumiCalcFileNames"     : Lumilist,
                                     } )
c.setalg("JetCalibrator", {
    "m_name"                   : "jetCalib_AntiKt4"+jetCont,
    "m_debug"                  : False,
    "m_inContainerName"        : "AntiKt4"+jetCont+"Jets",
    "m_jetAlgo"                : "AntiKt4"+jetCont.replace('LowPt',''),
    "m_outContainerName"       : "Jets_Calib",
    "m_outputAlgo"             : "JetsSyst_Calib",
    "m_sort"                   : True,
    "m_calibSequence"          : calibSeq,
    "m_calibConfigFullSim"     : calibConfig,
    "m_calibConfigData"        : calibConfig,
    "m_forceInsitu"            : False,
    "m_JESUncertConfig"        : "",
    "m_JESUncertMCType"        : "",
    "m_JERUncertConfig"        : "",
    "m_JERApplyNominal"        : False,
    "m_JERFullSys"             : False,
    "m_jetCleanCutLevel"       : "LooseBad",
    "m_jetCleanUgly"           : False,
    "m_cleanParent"            : False,
    "m_saveAllCleanDecisions"  : False,
    "m_redoJVT"                : True,
    "m_systName"               : "Nominal",
    "m_systVal"                : 0,
    "m_jetCalibToolsDEV"       : False,
    } )

c.setalg("JetSelector", {
    "m_name"                    :  "JetSelector",
    "m_debug"                   :  False,
    "m_inContainerName"         :  "Jets_Calib",
    "m_outContainerName"        :  "Jets_Selected",
    "m_inputAlgo"               :  "JetsSyst_Calib",
    "m_outputAlgo"              :  "JetsSyst_Selected",
    "m_createSelectedContainer" :  True,
    "m_decorateSelectedObjects" :  True,
    "m_jetScaleType"            :  "JetConstitScaleMomentum",
    #"m_pT_min"                  :  20e3,
    #"m_doJVT"                   :  True,
    "m_pT_min"                  :  7e3,
    "m_doJVT"                   :  False,
    "m_eta_max"                 :  4.8,
    "m_doBTagCut"               :  False,
    "m_cleanJets"               :  True,
    "m_cleanEvent"              :  True,
#    "m_cleaningDecoration"      : "HasBadBaselineJets",
    } )


##---------------------------------------------------------------------------------------------
### GSC Configuration  ###
c.setalg("GSCAlgo",   { "m_name"  : "GSC",

#### Jet collection and associated observables ####
  "m_inContainerName_jets"     : "Jets_Selected",
  #"m_inContainerName_jets"     : "Jets_Calib",
  "m_inContainerName_truth"    : "AntiKt4TruthJets",
  "m_jetDef"                : "AntiKt4"+jetCont.replace('LowPt',''),

### Plotting Options ###
  "m_eventDetailStr" : "pileup",
  "m_jetDetailStr" : "",
#  "m_jetDetailStr" : "kinematic truth truth_details flavorTag sfFTagVL sfFTagL sfFTagM sfFTagT",
  "m_trigDetailStr" : "basic passTriggers",

### Extra Options ###
  "m_debug"           : False,
  "m_dRmatching"      : True,
  "m_requireIso"      : True,
  "m_addTrackMoments" : False,

  } )

