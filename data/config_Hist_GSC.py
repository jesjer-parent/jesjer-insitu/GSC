import ROOT
import sys,os

from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

c.setalg("HistogramTree", {
  "m_name"                : "Hist",
  #If JZW (Pythia8), reweight to number of events, not sum of event weight
  "m_isJZW"               : True,            
  "m_msgLevel"            : "info",
  "m_applyIsolation"      : False,
  "m_jetType"             : "AntiKt4EMTopo",
  "m_GSCFile"             : "",
  "m_GSCOrder"            : "",
  "m_muRanges"            : "10,20,30,40,50,60,70",
  #"m_GSCFile"             : "GSC/data/GSC_R21_EM.root",
  #"m_GSCOrder"            : "Tile0,EM3,Ntrk,Wtrk,Nseg",
  # For flavor uncertainties
  #"m_etaBins"       :  "0,45",
  #"m_etaBins"       :  "0,3,8,12,17,25,28,32,36,45",
  } )

