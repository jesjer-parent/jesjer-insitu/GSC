// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// package include(s):
#include <xAODAnaHelpers/HelperFunctions.h>
#include <GSC/HistogramTree.h>
#include <xAODAnaHelpers/tools/ReturnCheck.h>
#include "xAODAnaHelpers/HistogramManager.h"


// ROOT include(s):
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TSystem.h"
#include "TKey.h"

#include <sstream>

// this is needed to distribute the algorithm to the workers
ClassImp(HistogramTree)

HistogramTree :: HistogramTree (std::string className) :
  Algorithm(className),
  jet_pt(0),
  jet_eta(0),
  jet_phi(0),
  jet_E(0),
  jet_DetEta(0),
  jet_Jvt(0),
  jet_PartonTruthLabelID(0),
  jet_true_pt(0),
  jet_true_eta(0),
  jet_true_phi(0),
  jet_true_e(0),
  jet_respE(0),
  jet_respPt(0),
  jet_ConstitE(0),
  jet_Wtrk1000(0),
  jet_Ntrk1000(0),
  jet_nMuSeg(0),
  jet_ChargedFraction(0),
  jet_EnergyPerSampling(0),
  jet_iso_halfPt(0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  Info("HistogramTree()", "Calling constructor");


}


EL::StatusCode HistogramTree :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  //
  Info("setupJob()", "Calling setupJob");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HistogramTree :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("histInitialize()", "Calling histInitialize");
  RETURN_CHECK("xAH::Algorithm::algInitialize()", xAH::Algorithm::algInitialize(), "");

  // Create histogram manager to do bookkeeping for us
  HistogramManager* HM = new HistogramManager("HistManager", "HistManager");

  // Kinematic histograms for X leading jets will be saved - does not affect GSC response distribtuions
  numHistJets = 4;

  //// Setup binnings to use, both pt & eta for regular corrections, E & coarse eta bins for Nseg
  // 45 eta bins of 0.1 size //
  if (m_etaBins == "Nominal" ){
    m_etaBins = "0";
    for(int iBin = 1; iBin <= 45; ++iBin){
      m_etaBins += ","+std::to_string(iBin);
    }
  }

  // Create vectors of float from the input binning //
  std::vector< std::string > tmpPt  = splitLine(m_ptBins);
  std::vector< std::string > tmpE  = splitLine(m_EBins);
  std::vector< std::string > tmpEta = splitLine(m_etaBins);
  std::vector< std::string > tmpEta_coarse = splitLine(m_etaBins_coarse);

  ANA_MSG_INFO( "Using eta bin edges of " << m_etaBins );
  ANA_MSG_INFO( "Using coarse eta bin edges of " << m_etaBins_coarse );
  ANA_MSG_INFO( "Using pt bin edges of " << m_ptBins );
  ANA_MSG_INFO( "Using E bin edges of " << m_EBins );

  Double_t ptEdgeArray[100];
  for( unsigned int iBin=0; iBin < tmpPt.size(); ++iBin ){
    ptEdgeArray[iBin] = std::stoi( tmpPt.at(iBin) );
    m_ptEdges.push_back( std::stof( tmpPt.at(iBin) ) );
  }

  Double_t energyEdgeArray[100];
  for( unsigned int iBin=0; iBin < tmpE.size(); ++iBin ){
    energyEdgeArray[iBin] = std::stoi( tmpE.at(iBin) );
    m_energyEdges.push_back( std::stof( tmpE.at(iBin) ) );
  }

  for( unsigned int iBin=0; iBin < tmpEta.size(); ++iBin ){
    m_etaEdges.push_back( std::stof( tmpEta.at(iBin)) /10. );
  }
  for( unsigned int iBin=0; iBin < tmpEta_coarse.size(); ++iBin ){
    m_etaEdges_coarse.push_back( std::stof( tmpEta_coarse.at(iBin)) /10. );
  }

  // Find mu & npv ranges for additional response histograms //
  std::vector< std::string > tmpmu = splitLine(m_muRanges);
  std::vector< std::string > tmpnpv = splitLine(m_npvRanges);

  for( unsigned int iBin=0; iBin < tmpmu.size(); ++iBin ){
    m_muEdges.push_back( std::stoi( tmpmu.at(iBin) ) );
  }
  for( unsigned int iBin=0; iBin < tmpnpv.size(); ++iBin ){
    m_npvEdges.push_back( std::stoi( tmpnpv.at(iBin) ) );
  }



  // Create inclusive & flavor response histograms for each eta bin //
  std::string dirName = "";
  std::string histName = "";
  for( unsigned int iEta=0; iEta < tmpEta.size()-1; ++iEta ){

    // Center pt histogram for x-axis remapping //
    histName = "p_inclusive_centerPt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    TProfile* thisCenterPt = new TProfile( (dirName+histName).c_str(), histName.c_str(), m_ptEdges.size()-1, ptEdgeArray, "g" );
    wk()->addOutput(thisCenterPt);
    h_centerPt.push_back( thisCenterPt );
    histName = "p_inclusive_centerE_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    TProfile* thisCenterE = new TProfile( (dirName+histName).c_str(), histName.c_str(), m_energyEdges.size()-1, energyEdgeArray, "g" );
    wk()->addOutput(thisCenterE);
    h_centerE.push_back( thisCenterE );

    histName = "h_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_response_vs_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_response_vs_E_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_response_vs_E.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    histName = "h_LQ_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_LQ_response_vs_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_LQ_response_vs_E_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_LQ_response_vs_E.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    histName = "h_gluon_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_gluon_response_vs_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_gluon_response_vs_E_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_gluon_response_vs_E.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    histName = "h_b_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_b_response_vs_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_b_response_vs_E_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_b_response_vs_E.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    histName = "h_c_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_c_response_vs_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_c_response_vs_E_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
    h_c_response_vs_E.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    // mu & NPV split histograms
    if (m_muEdges.size() > 0 ){
      std::vector< TH2F* > tmpmu_pt;
      for( unsigned int iBin=0; iBin < m_muEdges.size()-1; ++iBin){
        std::string muLow = std::to_string(m_muEdges.at(iBin));
        std::string muHigh = std::to_string(m_muEdges.at(iBin+1));
        histName = "h_mu"+muLow+"to"+muHigh+"_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
        tmpmu_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_mu_pt.push_back( tmpmu_pt );

      std::vector< TH2F* > tmpmu_e;
      for( unsigned int iBin=0; iBin < m_muEdges.size()-1; ++iBin){
        std::string muLow = std::to_string(m_muEdges.at(iBin));
        std::string muHigh = std::to_string(m_muEdges.at(iBin+1));
        histName = "h_mu"+muLow+"to"+muHigh+"_response_vs_e_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
        tmpmu_e.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5.) );
      }
      h_response_vs_mu_E.push_back( tmpmu_e );

    }

    // NPV split histograms
    if (m_npvEdges.size() > 0 ){
      std::vector< TH2F* > tmpnpv_pt;
      for( unsigned int iBin=0; iBin < m_npvEdges.size()-1; ++iBin){
        std::string npvLow = std::to_string(m_npvEdges.at(iBin));
        std::string npvHigh = std::to_string(m_npvEdges.at(iBin+1));
        histName = "h_npv"+npvLow+"to"+npvHigh+"_response_vs_pt_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
        tmpnpv_pt.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_npv_pt.push_back( tmpnpv_pt );

      std::vector< TH2F* > tmpnpv_e;
      for( unsigned int iBin=0; iBin < m_npvEdges.size()-1; ++iBin){
        std::string npvLow = std::to_string(m_npvEdges.at(iBin));
        std::string npvHigh = std::to_string(m_npvEdges.at(iBin+1));
        histName = "h_npv"+npvLow+"to"+npvHigh+"_response_vs_e_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1);
        tmpnpv_e.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5.) );
      }
      h_response_vs_npv_E.push_back( tmpnpv_e );

    }

    // Create the response histograms for each GSC variable if they are within the correct eta range //
    if( m_etaEdges.at(iEta) < m_maxEta_Tile0 ){
      std::vector< TH2F* > tmpTile0;
      for( unsigned int iPt=0; iPt < tmpPt.size()-1; ++iPt ){
        histName = "h_response_vs_Tile0_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1)+"_pt_"+tmpPt.at(iPt)+"_"+tmpPt.at(iPt+1);
        tmpTile0.push_back( HM->book(dirName.c_str(), histName.c_str(), "f_{Tile0}", 140, -0.1, 0.6, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_Tile0.push_back( tmpTile0 );
    }

    if( m_etaEdges.at(iEta) < m_maxEta_EM3 ){
      std::vector< TH2F* > tmpEM3;
      for( unsigned int iPt=0; iPt < tmpPt.size()-1; ++iPt ){
        histName = "h_response_vs_EM3_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1)+"_pt_"+tmpPt.at(iPt)+"_"+tmpPt.at(iPt+1);
        tmpEM3.push_back( HM->book(dirName.c_str(), histName.c_str(), "f_{EM3}", 80, -0.1, 0.3, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_EM3.push_back( tmpEM3 );
    }

    if( m_etaEdges.at(iEta) < m_maxEta_Ntrk ){
      std::vector< TH2F* > tmpNtrk;
      for( unsigned int iPt=0; iPt < tmpPt.size()-1; ++iPt ){
        histName = "h_response_vs_Ntrk_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1)+"_pt_"+tmpPt.at(iPt)+"_"+tmpPt.at(iPt+1);
        tmpNtrk.push_back( HM->book(dirName.c_str(), histName.c_str(), "N_{trk}", 50, 0, 50, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_Ntrk.push_back( tmpNtrk );
    }

    if( m_etaEdges.at(iEta) < m_maxEta_Wtrk ){
      std::vector< TH2F* > tmpWtrk;
      for( unsigned int iPt=0; iPt < tmpPt.size()-1; ++iPt ){
        histName = "h_response_vs_Wtrk_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1)+"_pt_"+tmpPt.at(iPt)+"_"+tmpPt.at(iPt+1);
        tmpWtrk.push_back( HM->book(dirName.c_str(), histName.c_str(), "W_{trk}", 100, 0, 0.5, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_Wtrk.push_back( tmpWtrk );
    }

    if( m_etaEdges.at(iEta) < m_maxEta_CF ){
      std::vector< TH2F* > tmpCF;
      for( unsigned int iPt=0; iPt < tmpPt.size()-1; ++iPt ){
        histName = "h_response_vs_CF_eta_"+tmpEta.at(iEta)+"_"+tmpEta.at(iEta+1)+"_pt_"+tmpPt.at(iPt)+"_"+tmpPt.at(iPt+1);
        tmpCF.push_back( HM->book(dirName.c_str(), histName.c_str(), "Charge Fraction", 110, -0.1, 1.0, "Jet p_{T} response", 200, 0., 2.) );
      }
      h_response_vs_CF.push_back( tmpCF );
    }
  }// for iEta

  // Now repeat for coarse eta bin
  for( unsigned int iEta=0; iEta < tmpEta_coarse.size()-1; ++iEta ){
    // Center pt histogram for x-axis remapping //
    histName = "p_inclusive_centerPt_eta_"+tmpEta_coarse.at(iEta)+"_"+tmpEta_coarse.at(iEta+1);
    TProfile* thisCenterPt = new TProfile( (dirName+histName).c_str(), histName.c_str(), m_ptEdges.size()-1, ptEdgeArray, "g" );
    wk()->addOutput(thisCenterPt);
    h_centerPt_coarse.push_back( thisCenterPt );
    histName = "p_inclusive_centerE_eta_"+tmpEta_coarse.at(iEta)+"_"+tmpEta_coarse.at(iEta+1);
    TProfile* thisCenterE = new TProfile( (dirName+histName).c_str(), histName.c_str(), m_ptEdges.size()-1, ptEdgeArray, "g" );
    wk()->addOutput(thisCenterE);
    h_centerE_coarse.push_back( thisCenterE );

    histName = "h_response_vs_pt_eta_"+tmpEta_coarse.at(iEta)+"_"+tmpEta_coarse.at(iEta+1);
    h_response_vs_pt_coarse.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet p_{T} [GeV]", m_ptEdges.size()-1, ptEdgeArray, "Jet p_{T} response", 200, 0., 2.) );
    histName = "h_response_vs_E_eta_"+tmpEta_coarse.at(iEta)+"_"+tmpEta_coarse.at(iEta+1);
    h_response_vs_E_coarse.push_back( HM->book(dirName.c_str(), histName.c_str(), "True jet E [GeV]", m_energyEdges.size()-1, energyEdgeArray, "Jet E response", 250, 0., 5. ) );

    // Create the Nseg response histograms for each GSC variavle if they are within the correct eta range //
    if( m_etaEdges_coarse.at(iEta) < m_maxEta_Nseg ){
      std::vector< TH2F* > tmpNseg;
      for( unsigned int iE=0; iE < tmpE.size()-1; ++iE ){
        histName = "h_response_vs_Nseg_eta_"+tmpEta_coarse.at(iEta)+"_"+tmpEta_coarse.at(iEta+1)+"_E_"+tmpE.at(iE)+"_"+tmpE.at(iE+1);
        tmpNseg.push_back( HM->book(dirName.c_str(), histName.c_str(), "N_{seg}", 100, 0, 200, "Jet E response", 250, 0., 5.) );
      }
      h_response_vs_Nseg.push_back( tmpNseg );
    }
  }// for Coarse iEta

  // Other cool histograms to save //
  h_NPV = HM->book(dirName.c_str(), "h_NPV", "", 100, 0, 100);
  h_averageInteractionsPerCrossing = HM->book(dirName.c_str(), "h_averageInteractionsPerCrossing", "", 100, 0, 100);
  h_correct_mu = HM->book(dirName.c_str(), "h_correct_mu", "", 100, 0, 100);
  h_njet = HM->book(dirName.c_str(), "h_njet", "n_{jet}", 12, 0, 12);

  h_jet_pt_all = HM->book(dirName.c_str(), "h_jet_pt_all", "Jet p_{T}", 100, 0, 2000);
  h_jet_eta_all = HM->book(dirName.c_str(), "h_jet_eta_all", "Jet #eta", 80, -4, 4);
  h_jet_phi_all = HM->book(dirName.c_str(), "h_jet_phi_all", "Jet #phi", 64, -TMath::Pi(), TMath::Pi() );
  h_jet_E_all = HM->book(dirName.c_str(), "h_jet_E_all", "Jet energy", 100, 0, 2000);

  // Per-jet histograms to save //
  for(unsigned int iE=0; iE < numHistJets; ++iE){
    vh_jet_pt.push_back( HM->book(  dirName.c_str(), ("h_jet_pt_"+std::to_string(iE)).c_str(), ("Jet p_{T} "+std::to_string(iE)).c_str(), 100, 0, 2000) );
    vh_jet_eta.push_back( HM->book( dirName.c_str(), ("h_jet_eta_"+std::to_string(iE)).c_str(), ("Jet #eta "+std::to_string(iE)).c_str(), 80, -4, 4) );
    vh_jet_phi.push_back( HM->book( dirName.c_str(), ("h_jet_phi_"+std::to_string(iE)).c_str(), ("Jet #phi "+std::to_string(iE)).c_str(), 64, -TMath::Pi(), TMath::Pi()) ) ;
    vh_jet_E.push_back( HM->book(   dirName.c_str(), ("h_jet_E_"+std::to_string(iE)).c_str(), ("Jet energy"+std::to_string(iE)).c_str(), 100, 0, 2000) );
    vh_jet_DetEta.push_back( HM->book( dirName.c_str(), ("h_jet_DetEta_"+std::to_string(iE)).c_str(), ("Jet #eta_{det} "+std::to_string(iE)).c_str(), 80, -4, 4) );
    vh_jet_Jvt.push_back( HM->book( dirName.c_str(), ("h_jet_Jvt_"+std::to_string(iE)).c_str(), ("Jet JVT "+std::to_string(iE)).c_str(), 100, 0, 1.) );
    vh_jet_PartonTruthLabelID.push_back( HM->book( dirName.c_str(), ("h_jet_PartonTruthLabelID_"+std::to_string(iE)).c_str(), ("Jet PartonTruthLabelID "+std::to_string(iE)).c_str(), 20, 0, 20) );
  }

  //Add all histograms to EventLoop output
  HM->record( wk() );

  Info("histInitialize()", "Histograms initialized!");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HistogramTree :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  Info("fileExecute()", "Calling fileExecute");

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode HistogramTree :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  Info("fileExecute()", "Changing input");

  //// Get number of events ////
  TFile* inputFile = wk()->inputFile();

  TH1F* cutflow = (TH1F*) inputFile->Get("MetaData_EventCount");
  //If Pythia8 reweight to number of events, else to sum of weights
  if( m_isJZW )
    m_totalNumEvents = cutflow->GetBinContent(1); //numberOfEvents
  else
    m_totalNumEvents = cutflow->GetBinContent(3);  //sumOfWeights

  Info("changeInput()", "In cutflow found initial number of events %f", m_totalNumEvents);

  ///// Connect branches ////
  TTree *tree = wk()->tree();
  tree->SetBranchStatus ("*", 1);


  tree->SetBranchAddress("runNumber", &runNumber);
  tree->SetBranchAddress("eventNumber", &eventNumber);
  tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
  tree->SetBranchAddress("NPV", &NPV);
  tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  tree->SetBranchAddress("correct_mu", &correct_mu);
  tree->SetBranchAddress("weight", &weight);
  tree->SetBranchAddress("njet", &njet);

  tree->SetBranchAddress("jet_EnergyPerSampling", &jet_EnergyPerSampling);
  tree->SetBranchAddress("jet_iso_halfPt", &jet_iso_halfPt);

  tree->SetBranchAddress("jet_pt", &jet_pt);
  tree->SetBranchAddress("jet_eta", &jet_eta);
  tree->SetBranchAddress("jet_phi", &jet_phi);
  tree->SetBranchAddress("jet_E", &jet_E);
  tree->SetBranchAddress("jet_DetEta", &jet_DetEta);
  tree->SetBranchAddress("jet_Jvt", &jet_Jvt);
  tree->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID);
  tree->SetBranchAddress("jet_ConstitE", &jet_ConstitE);
  tree->SetBranchAddress("jet_true_pt", &jet_true_pt);
  tree->SetBranchAddress("jet_true_eta", &jet_true_eta);
  tree->SetBranchAddress("jet_true_phi", &jet_true_phi);
  tree->SetBranchAddress("jet_true_e", &jet_true_e);
  tree->SetBranchAddress("jet_respE", &jet_respE);
  tree->SetBranchAddress("jet_respPt", &jet_respPt);
  tree->SetBranchAddress("jet_Wtrk1000", &jet_Wtrk1000);
  tree->SetBranchAddress("jet_Ntrk1000", &jet_Ntrk1000);
  tree->SetBranchAddress("jet_nMuSeg", &jet_nMuSeg);
  tree->SetBranchAddress("jet_ChargedFraction", &jet_ChargedFraction);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  Info("initialize()", "Initializing HistogramTree... ");

  m_eventCounter = -1;
  ANA_CHECK( loadGSC() );

  Info("initialize()", "HistogramTree succesfully initialized!");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_VERBOSE("execute, Histogram B-L Tree");

  ++m_eventCounter;

  wk()->tree()->GetEntry (wk()->treeEntry());

  weight = weight/m_totalNumEvents; // 1fb = 1pb*1000

  if( std::isnan(weight) ){
    ANA_MSG_WARNING("Warning!!!!! it's nan!");
    return EL::StatusCode::SUCCESS;
  }

  ANA_MSG_VERBOSE("In execute, fill regular histograms");

  for(unsigned int iJ=0; iJ < jet_pt->size(); ++iJ){

    //Apply isolation branch, may not be needed in applied directly in TTree code
    if( m_applyIsolation && !jet_iso_halfPt->at(iJ) )
      continue;

    // Calculate sumEt of the energy samplings
    float sumEt = 0.;
    for( unsigned int iEt=0; iEt < jet_EnergyPerSampling->at(iJ).size(); ++iEt ){
      sumEt += jet_EnergyPerSampling->at(iJ).at(iEt);
    }

    float thisTile0 = (jet_EnergyPerSampling->at(iJ).at(12)+jet_EnergyPerSampling->at(iJ).at(18)) / jet_ConstitE->at(iJ);
    float thisEM3 = (jet_EnergyPerSampling->at(iJ).at(3)+jet_EnergyPerSampling->at(iJ).at(7)) / jet_ConstitE->at(iJ);
    float thisNtrk = jet_Ntrk1000->at(iJ);
    float thisWtrk = jet_Wtrk1000->at(iJ);
    float thisCF = jet_ChargedFraction->at(iJ);
    float thisNseg = jet_nMuSeg->at(iJ);

    // Create calibration jet object
    xAOD::JetFourMom_t calibJet;
    TLorentzVector TLVjet;
    TLVjet.SetPtEtaPhiE( jet_pt->at(iJ), jet_eta->at(iJ), jet_phi->at(iJ), jet_E->at(iJ) );
    calibJet.SetPxPyPzE( TLVjet.Px(), TLVjet.Py(), TLVjet.Pz(), TLVjet.E() );

    // Apply GSC calibrations
    if( m_GSCEnums.size() > 0 )
      ANA_CHECK( applyGSC(&calibJet, jet_DetEta->at(iJ), thisTile0, thisEM3, thisNtrk, thisWtrk, thisCF, thisNseg) );

    jet_respPt->at(iJ) = calibJet.Pt() / jet_true_pt->at(iJ);
    jet_respE->at(iJ) = calibJet.E() / jet_true_e->at(iJ);

    // Get bin indicies for pt, E, and eta (additional -1 makes it vector index) //
    int ptBin = std::upper_bound( m_ptEdges.begin(), m_ptEdges.end(), jet_true_pt->at(iJ) ) - m_ptEdges.begin() -1;
    int EBin = std::upper_bound( m_energyEdges.begin(), m_energyEdges.end(), jet_true_e->at(iJ) ) - m_energyEdges.begin() -1;
    int etaBin = std::upper_bound( m_etaEdges.begin(), m_etaEdges.end(), fabs(jet_DetEta->at(iJ)) ) - m_etaEdges.begin() -1;
    int etaBin_coarse = std::upper_bound( m_etaEdges_coarse.begin(), m_etaEdges_coarse.end(), fabs(jet_DetEta->at(iJ)) ) - m_etaEdges_coarse.begin() -1;

    if( etaBin < 0 || etaBin >= (int)m_etaEdges.size() -1 )
      continue;

    // Fill general histgorams //
    h_response_vs_pt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
    h_response_vs_E.at(etaBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
    h_centerPt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_true_pt->at(iJ), weight);
    h_centerE.at(etaBin)->Fill(jet_true_e->at(iJ), jet_true_e->at(iJ), weight);

    if( (unsigned int) etaBin_coarse < h_response_vs_pt_coarse.size() ){
      h_response_vs_pt_coarse.at(etaBin_coarse)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
      h_response_vs_E_coarse.at(etaBin_coarse)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
      h_centerPt_coarse.at(etaBin_coarse)->Fill(jet_true_pt->at(iJ), jet_true_pt->at(iJ), weight);
      h_centerE_coarse.at(etaBin_coarse)->Fill(jet_true_e->at(iJ), jet_true_e->at(iJ), weight);
    }

    // Fill flavor response histograms
    if( jet_PartonTruthLabelID->at(iJ) >= 1 && jet_PartonTruthLabelID->at(iJ) <= 3 ){
      h_LQ_response_vs_pt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
      h_LQ_response_vs_E.at(etaBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
    }else if( jet_PartonTruthLabelID->at(iJ) == 9 || jet_PartonTruthLabelID->at(iJ) == 21 ){
      h_gluon_response_vs_pt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
      h_gluon_response_vs_E.at(etaBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
    }else if( jet_PartonTruthLabelID->at(iJ) == 5 ){
      h_b_response_vs_pt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
      h_b_response_vs_E.at(etaBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
    }else if( jet_PartonTruthLabelID->at(iJ) == 4 ){
      h_c_response_vs_pt.at(etaBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
      h_c_response_vs_E.at(etaBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
    }

    // Fill mu histograms
    if (m_muEdges.size() > 0 ){
      int muBin = std::upper_bound( m_muEdges.begin(), m_muEdges.end(), averageInteractionsPerCrossing ) - m_muEdges.begin() -1;
      if (muBin >= 0 && muBin < (int)h_response_vs_mu_pt.at(etaBin).size() ){
        h_response_vs_mu_pt.at(etaBin).at(muBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
        h_response_vs_mu_E.at(etaBin).at(muBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
      }
    }
    // Fill NPV histograms
    if (m_npvEdges.size() > 0 ){
      int npvBin = std::upper_bound( m_npvEdges.begin(), m_npvEdges.end(), NPV ) - m_npvEdges.begin() -1;
      if (npvBin >= 0 && npvBin < (int)h_response_vs_npv_pt.at(etaBin).size() ){
        h_response_vs_npv_pt.at(etaBin).at(npvBin)->Fill(jet_true_pt->at(iJ), jet_respPt->at(iJ), weight);
        h_response_vs_npv_E.at(etaBin).at(npvBin)->Fill(jet_true_e->at(iJ), jet_respE->at(iJ), weight);
      }
    }

    // Fill response hists against a specific variable if it's within the pt (E) and eta ranges //
    if( ptBin >= 0 && ptBin < (int)m_ptEdges.size()-1 ){

      if( etaBin < (int) h_response_vs_Tile0.size() ){
        h_response_vs_Tile0.at(etaBin).at(ptBin)->Fill(thisTile0, jet_respPt->at(iJ), weight);
      }

      if( etaBin < (int) h_response_vs_EM3.size() ){
        h_response_vs_EM3.at(etaBin).at(ptBin)->Fill(thisEM3, jet_respPt->at(iJ), weight);
      }

      if( etaBin < (int) h_response_vs_Ntrk.size() ){
        h_response_vs_Ntrk.at(etaBin).at(ptBin)->Fill(thisNtrk, jet_respPt->at(iJ), weight);
      }

      if( etaBin < (int) h_response_vs_Wtrk.size() ){
        // Should skip events where thisWtrk = -1, but should be fine in underflow bin
        h_response_vs_Wtrk.at(etaBin).at(ptBin)->Fill(thisWtrk, jet_respPt->at(iJ), weight);
      }

      if( etaBin < (int) h_response_vs_CF.size() ){
        h_response_vs_CF.at(etaBin).at(ptBin)->Fill(thisCF, jet_respPt->at(iJ), weight);
      }
    }//within pt range

    if( EBin >= 0 && EBin < (int)m_energyEdges.size()-1 ){

      if( etaBin_coarse < (int)h_response_vs_Nseg.size() ){
        h_response_vs_Nseg.at(etaBin_coarse).at(EBin)->Fill(thisNseg, jet_respE->at(iJ), weight);
      }
    }//within E range

  }//for jet

  ANA_MSG_VERBOSE("execute, fill generic jet histograms");

  // Fill other fun histograms
  h_NPV->Fill( NPV, weight );
  h_averageInteractionsPerCrossing->Fill( averageInteractionsPerCrossing, weight );
  h_correct_mu->Fill( correct_mu, weight );
  h_njet->Fill( njet, weight );

  // Fill leading jet histograms
  for(unsigned int iJ=0; iJ < jet_pt->size(); ++iJ){
    h_jet_pt_all->Fill( jet_pt->at(iJ), weight );
    h_jet_eta_all->Fill( jet_eta->at(iJ), weight );
    h_jet_phi_all->Fill( jet_phi->at(iJ), weight );
    h_jet_E_all->Fill( jet_E->at(iJ), weight );

    if( iJ < numHistJets){
      vh_jet_pt.at(iJ)->Fill( jet_pt->at(iJ), weight );
      vh_jet_eta.at(iJ)->Fill( jet_eta->at(iJ), weight );
      vh_jet_phi.at(iJ)->Fill( jet_phi->at(iJ), weight );
      vh_jet_E.at(iJ)->Fill( jet_E->at(iJ), weight );
      vh_jet_DetEta.at(iJ)->Fill( jet_DetEta->at(iJ), weight );
      vh_jet_Jvt.at(iJ)->Fill( jet_Jvt->at(iJ), weight );
      vh_jet_PartonTruthLabelID.at(iJ)->Fill( jet_PartonTruthLabelID->at(iJ), weight );
    }
  }//end jets

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistogramTree :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  Info("finalize()", "Number of processed events \t= %i", m_eventCounter);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  RETURN_CHECK("xAH::Algorithm::algFinalize()", xAH::Algorithm::algFinalize(), "");
  return EL::StatusCode::SUCCESS;
}

// Get vector of selections, splitting by commas
std::vector< std::string  > HistogramTree::splitLine( std::string inLine, char sep/*=','*/ ){
  std::vector< std::string > lineVec;
  std::istringstream ss(inLine);
  std::string subStr = "";
  while( std::getline(ss, subStr, sep) ){
    lineVec.push_back( subStr );
  }
  return lineVec;
}


EL::StatusCode HistogramTree :: loadGSC(){
  ANA_MSG_INFO("loadGSC");


  // Load allowed correction enums //
  std::vector<std::string> GSCNames = {"Tile0", "EM3", "Ntrk", "Wtrk", "CF", "Nseg"};
  std::map< std::string, GSCType > GSCEnums;
  GSCEnums["Tile0"] = Tile0;
  GSCEnums["EM3"]   = EM3;
  GSCEnums["Ntrk"]  = Ntrk;
  GSCEnums["Wtrk"]  = Wtrk;
  GSCEnums["CF"]    = CF;
  GSCEnums["Nseg"]  = Nseg;

  if( m_GSCFile == "" ){
    ANA_MSG_INFO("No GSC file given, will not apply an initial calibration.");
    return EL::StatusCode::SUCCESS;
  }

  TFile* GSCFile = TFile::Open(PathResolverFindCalibFile(m_GSCFile).c_str(), "READ");
  if( !GSCFile ){
    ANA_MSG_ERROR( "Could not find input GSC calibration file "<< m_GSCFile << ", exiting");
    return EL::StatusCode::FAILURE;
  }

  // Load the histograms from an input file
  std::vector<std::string> GSCOrderVec = splitLine(m_GSCOrder, ',');
  for( auto thisGSCString : GSCOrderVec ){
    ANA_MSG_INFO("Applying GSC for variable " << thisGSCString);
    GSCType thisGSCVar = GSCEnums[ thisGSCString ];
    if( thisGSCVar == NoGSCType ){
      Error("loadGSC", "Could not find GSC correction of type %s, exiting.", thisGSCString.c_str() );
      return EL::StatusCode::FAILURE;
    }
    m_GSCEnums.push_back( thisGSCVar );

    std::vector< TH2F* > tmpHists;
    std::vector< std::pair<float, float> > tmpEdges;

    std::vector<float> thisEtaEdges;
    if( thisGSCVar == Nseg ){
      thisEtaEdges = m_etaEdges_coarse;
    } else {
      thisEtaEdges = m_etaEdges;
    }

    for(unsigned int iEta = 0; iEta < thisEtaEdges.size()-1; ++iEta){
      std::string lowBinEdge = std::to_string(int(thisEtaEdges.at(iEta)*10));
      std::string thisHistName = m_jetType+"_"+thisGSCString+"_interpolation_resp_eta_"+lowBinEdge;
      //Only add this eta region if it exists for this variable
      if( GSCFile->GetListOfKeys()->Contains(thisHistName.c_str()) ){
        tmpHists.push_back( (TH2F*) GSCFile->Get( thisHistName.c_str()) );
        tmpHists.at(iEta)->SetDirectory(0);

        //Figure out eta bin edges
        std::pair<float, float> thisPair = std::make_pair( thisEtaEdges.at(iEta), thisEtaEdges.at(iEta+1) );
        tmpEdges.push_back(thisPair);
      }

    }
    m_GSCHists_eta[thisGSCVar] = tmpHists;
    m_GSCEtaEdges[thisGSCVar] = tmpEdges;

  }


  GSCFile->Close();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistogramTree :: applyGSC(xAOD::JetFourMom_t *calibJet, float detEta, float thisTile0, float thisEM3, float thisNtrk, float thisWtrk, float thisCF, float thisNseg){

  float correction = 1.;

  // Loop over all GSC variables and apply them
  for( auto thisGSCVar : m_GSCEnums ){

    // Get new Pt (or energy) from the last GSC calibration steps
    float thisPt = 0.;
    if( thisGSCVar == Nseg ){
      thisPt = calibJet->e()*correction;
    } else {
      thisPt = calibJet->pt()*correction;
    }

    // Choose which GSC variable is applied next
    float thisVar = 0.;
    if( thisGSCVar == Tile0 ){
      thisVar = thisTile0;
      ANA_MSG_DEBUG( "Applying Tile0");
    }else if( thisGSCVar == EM3 ){
      thisVar = thisEM3;
      ANA_MSG_DEBUG( "Applying EM3");
    }else if( thisGSCVar == Ntrk ){
      thisVar = thisNtrk;
      ANA_MSG_DEBUG( "Applying Ntrk");
    }else if( thisGSCVar == Wtrk ){
      thisVar = thisWtrk;
      ANA_MSG_DEBUG( "Applying Wtrk");
      // Do not apply when Wtrk is less than zero
      if (thisVar < 0)
        continue;
    }else if( thisGSCVar == CF ){
      thisVar = thisCF;
      ANA_MSG_DEBUG( "Applying CF");
    }else if( thisGSCVar == Nseg ){
      thisVar = thisNseg;
      ANA_MSG_DEBUG( "Applying Nseg");
      //Don't correct when less than 20 segments
      if( thisVar < 20 )
        continue;
    }

    // Find eta bin, and skip correction if it's out of this GSC correction's eta range //
    int iEta = -1;
    float thisEta = fabs(detEta);
    for( unsigned int iEtaBin = 0; iEtaBin < m_GSCEtaEdges[thisGSCVar].size(); ++iEtaBin ){
      if( thisEta >= m_GSCEtaEdges[thisGSCVar].at(iEtaBin).first && thisEta < m_GSCEtaEdges[thisGSCVar].at(iEtaBin).second ){
        iEta = iEtaBin;
        break;
      }
    }
    if (iEta == -1)
      continue;


    // Find jet pt bin
    if( thisPt >= m_GSCHists_eta[thisGSCVar].at(iEta)->GetXaxis()->GetBinCenter( m_GSCHists_eta[thisGSCVar].at(iEta)->GetNbinsX() ) )
      thisPt = m_GSCHists_eta[thisGSCVar].at(iEta)->GetXaxis()->GetBinCenter( m_GSCHists_eta[thisGSCVar].at(iEta)->GetNbinsX() ) - 1e-6;
    else if( thisPt <= m_GSCHists_eta[thisGSCVar].at(iEta)->GetXaxis()->GetBinCenter( 1 ) )
      thisPt = m_GSCHists_eta[thisGSCVar].at(iEta)->GetXaxis()->GetBinCenter( 1 )+ 1e-6;


    // Find bin for this GSC variable
    if( thisVar >= m_GSCHists_eta[thisGSCVar].at(iEta)->GetYaxis()->GetBinCenter( m_GSCHists_eta[thisGSCVar].at(iEta)->GetNbinsY() ) )
      thisVar = m_GSCHists_eta[thisGSCVar].at(iEta)->GetYaxis()->GetBinCenter( m_GSCHists_eta[thisGSCVar].at(iEta)->GetNbinsY() ) - 1e-6;
    else if( thisVar <= m_GSCHists_eta[thisGSCVar].at(iEta)->GetYaxis()->GetBinCenter( 1 ) )
      thisVar = m_GSCHists_eta[thisGSCVar].at(iEta)->GetYaxis()->GetBinCenter( 1 ) +1e-6;

    // Get new correction
    correction *= 1./m_GSCHists_eta[thisGSCVar].at(iEta)->Interpolate(thisPt, thisVar);


    ANA_MSG_DEBUG("For pt/deteta " << thisPt << "/" << detEta << " has a var " << thisVar << " which gives a correction " << m_GSCHists_eta[thisGSCVar].at(iEta)->Interpolate(thisPt, thisVar) << " for a total of " << correction);
  }
  // apply full calibration to jet
  (*calibJet) *= correction;

  return EL::StatusCode::SUCCESS;
}
