This package derives the global sequential correction (GSC) for the ATLAS jet energy scale calibration.
(The obsolete twiki for the previous version of this code may be found at https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/DeriveMCJES2015#3_DeriveGSC.)

This code requires the xAODAnaHelpers package, which may be downloaded with:
git clone https://github.com/UCATLAS/xAODAnaHelpers.git

You should use the master branch of xAODAnaHelpers.  But if there are any issues, the last known working version for R21 recommendations is:
git checkout tags/GSC-Nov28-21.2.10

This code also requires the JES_ResponseFitter package for fitting:
git clone https://:@gitlab.cern.ch:8443/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter.git

All packages should be placed in the source directory.

Example setup for your area for AnalysisBase 21.2.18.  You should use the [latest recommended AB 21 version](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease).
mkdir source
cd source
//checkout all necessary packages in source
asetup AnalysisBase,21.2.18,here
mkdir ../build
cd ../build
cmake ../source
make
source x86_64-slc6-gcc62-opt/setup.sh
cd ../source

All commands should be run from the source directory, where AnalysisBase was first set up and where the GSC/ and xAODAnaHelpers/ directories are located.


#------- Step 1: Getting TTree of isolated, truth-matched jets ---------------#

The analysis code runs over JETM1 dijet MC and outputs TTrees with the required kinematic and GSC variables. 
The code for creating the TTrees is now [IsolatedJetTree](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree).
This is an Eventloop algorithm that is run via xAODAnaHelpers through the xAH_run.py script.  
This is a fairly complex script with a large deal of functionality, so don't worry about understanding the details of how it works!  
The EventLoop algorthims that are run, and their variable settings, are given in IsolatedJetTree/data/config_Tree.py, which defines the algorithms to run and their order. 
When running the GSC, make sure to have "GSCVars" in "m_jetDetailStr" to ensure you save all the relevant GSC variables.
This uses xAODAnaHelpers for the event-level selections and to calibrate and select the reconstructed jets. 
It then runs IsolatedJetTree/Root/IsolatedJetAlgo.cxx to match isolated truth and reconstructed jets.

Run an example job locally with a command like:
python xAH_run.py --config source/IsolatedJetTree/data/config_Tree.py --files path/to/DAODFile.root --force direct

To run over all JZ slices through the grid submission, use the following:
python source/IsolatedJetTree/scripts/runGrid.py

The containers used by this script and some details on the grid submission (i.e. using production rights) are hardcoded in this script, and should be updated by the user.

#------- Step 2: Download TTree output ---------------#

The grid output samples can be downloaded locally with the downloadAndMerge script.  The output files are TTrees, and can be downloaded with a command such as:

`python GSC/scripts/downloadAndMerge.py --types tree --container user.jdandoy.36*.Pythia_EM4_300517`

The output will be placed in the gridOutput/tree/ directory.  The unmerged files will be stored in gridOutput/rawDownload, and may be deleted.
The files for each slice will be merged automatically, but the various slices will remain separate.

#------- Step 3: Histogram Output ---------------#
The individual TTree slices are reweighted and histogrammed using the data/config_Hist_GSC.py config file. This runs the Root/HistogramTree.cxx algorithm to apply GSC calibrations to the jets and to histogram the TTree.

The attribute "m_GSCOrder" determines the GSC stages to be applied locally, and should be set to empty for the first iteration.
After each iteration of the code a new variable should be added, and after 5 iterations "m_GSCOrder" should be "Tile0,EM3,Ntrk,Wtrk,Nseg" for EMTopo jets.
The calibration histograms for each GSC stage are derived below, and are the root output of the smoothing step.
This root file should be placed in GSC/data/ folder, and it's path and name should be specified as "m_GSCFile" (e.g. "GSC/data/GSCcalib_21.root").
Each iteration of the code will only produce calibrations histograms for one stage, so for subsequent stages the correct root files must be hadd'd together into a single file.
For example, when running the 3rd stage for Ntrk, "m_GSCOrder" should be set to "Tile0,EM3" to apply the Tile0 and EM3 calibrations, and "m_GSCFile" should point to a file that is the hadd'd combination of Smoothed_Fitted_EM3.root and Smoothed_Fitted_Tile0.root, the output of the last smoothing stage.

HistogramTree can be run for an individual slice with :
`./xAODAnaHelpers/scripts/xAH_run.py -f --treeName GSC_tree --files submitDir/data-tree/mc16_13TeV.root --submitDir gridOutput/localJobs/mc16_13TeV_NewStudy --config GSC/data/config_Hist_DataMC.py direct`

It can also be run over several TTree samples in parallel with runLocalHistogrammer.py.  For example:
`python GSC/scripts/runLocalHistogrammer.py --ncores 4 --outTag Tile0 --path gridOutput/final_r21EM/tree/`

The output of runLocalHistogrammer.py with be separate for the various JZ slices and should be combined with hadd into a single root file for further fitting and smoothing.


#------- Step 3: Response fitting ---------------#
The response of the combined histograms is then fit with gaussian functions to determine the centeral value of the response.
It is fit in bins of truth pt and eta, and it is fit in both in bins of the VAR (e.g. Tile0 or Ntrk) and inclusively in the VAR.
The goal of the GSC is to not change the overall response, so every fit in a bin of VAR is divided by the result of the inclusive VAR fit to ensure the inclusive VAR fit does not change as each stage progresses.
Common VAR include Tile0, EM3, nTrk, trackWIDTH, PunchThrough, and chargedFraction, but the code does not care about which VAR is being fit, as long as the histograms are created properly.

The GSCFit.py fitting code can be run with a command like:
`python GSC/scripts/GSCfitter.py -b --correction Tile0 --input gridOutput/Tile0/EM4_Tile0.root`

Here the correction is the VAR to be used, and is given for the naming convention of the output file and to automatically set the special binning and kinematic variable for the Nseg correction.
The correction "Inclusive" may be used to run just the inclusive fits, over the entire eta range up to 4.5.
Expert-level options include:
--numXbins : set the number of bins to be used for VAR.  Set to 10 by default, but set to 11 for Tile0 due to the unique negative- and zero-value binning.
--kinematic : The kinematic variable to be used.  Typically pt, though E is used for the Nseg correction.
Eta ranges may be specified with --etaMin and --etaMax for testing, with the given value corresponding to the bin number, but in general the code will identify the number of eta bins from the input file and fit them all.

After running the GSCFit.py, you should have a new file called Fitted_VAR_FILENAME.root, where FILENAME is the name of the input variable and VAR is determined by --correction.
A plot dump of all fits is also given in PlotDump_Fitter_response_VAR.pdf for the inclusive fits and in PlotDump_Fitter_Var_VAR.pdf for the VAR-dependent fits.

For each VAR, it is generally useful to run GSCfitter.py twice, once for the previous calibration stage for closure tests and once for the next stage for calibrations.
For example, for EM3, it is useful to run the Tile0 fit to check the closure of the Tile0 calibration, and to run EM3 to derive the next step.
For the last calibration stage, it is useful to run fits for all variables to check the final closure of them all.


#------- Step 3: Response smoothing ---------------#
The derived response is then smoothed as a function of pt (or E) and the VAR using GSCsmoother.py.
Similarly to the fitting, the smoother will identify most relevant information from the file itself, and the --correction option should be set just for naming conventions.
It can be run with a command such as:
`python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input gridOutput/final_r21EM/Tile0/Fitted_Tile0_Tile0.root`

The expert-level options include:
--jetType to set the name of the jet type.  This is required for proper naming convention for JetCalibToolsonly.  Set to AntiKt4EMTopo by default.

The output are smoothed calibration histograms (both TH2Fs for each eta bin and a combined TH3F) in Smoothed_FILENAME.root, where FILENAME is the name of the input file.
This output is the final calibration file, and should be hadd'd with any previous calibration files and used as "m_GSCFile" by the Histogrammer code.
Plots are also dumped in Smoothed_plots_VAR.pdf.

#------- Step 4: Plot!  ---------------#

Three plotting scripts exist in scripts/ for various plots types.

plotGSCResponse.py plots the usual pt response curves verses VAR for several pt regions in a given eta bin.
These are the figures shown in 7 TeV and 13 TeV publications.
It is run with a command like:
`python GSC/scripts/plotGSCResponse.py -b --input gridOutput/final_r21EM/Tile0/Fitted_Tile0_Tile0.root --correction Tile0`

The input is the fitted root file that is the output of GSCfitter.py.
Output is saved in a plots/ directory, placed inside the input directory.
Optional commands include:
--pts : Comma separated list of pt ranges to draw, nominally set to "350_400,80_100,30_40", or for Nseg set to "1600_2000,1000_1200,600_800".

plotSmoothedCurves.py plots the smoothed response curves as a function of pt or of eta, and is ismilar to plotGSCResponse.py.
The input is the smoothed histograms that are the output of GSCsmoother.py.
The --pts and --etas options work similarly to plotGSCResponse.py, and are "all" by default.

plotGSCImprovement.py plots the improvement of the central response and resolution across different GSC stages (as opposed to the previous 2 plotting scripts, which each dealt with a single stage at a given iteration).
The input files (inputFiles), the name of their stages (corrTypes), and their colors (fileColors) are each hardcoded at the beginning of the code, and should be set by the user.
This code also runs over differnt flavor types, including light-quarks, gluons, b-quarks, c-quarks, and inclusively.
The output plots are put in the plots/ directory in the same directory that the code is run in.

#------------ Extra options and studies ----------------#

dR matching between truth and reco jets is the default.
GhostTruthAssociation matching can be used by setting m_dRmatching to false.

There is functionality to study various isolation working points.
By setting m_requireIso to false in your local version of config_Tree_GSC.py, only the loosest isolation requirement will be applied, and the various per-jet isolation decisions will be saved in the TTree.
The default working point (halfPt) can then be applied by setting m_applyIsolation in config_Hist_GSC.py.
Testing other working point will require small hardcoded changes in HistogramTree.cxx.

Ntrk & Wtrk can be studied with higher pt thresholds (2, 3, and 4 GeV) by setting m_addTrackMoments to true.
