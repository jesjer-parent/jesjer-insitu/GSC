import ROOT

inputStr = "GSCcalib_21.root"
inputFile = ROOT.TFile.Open(inputStr, "READ")

outputStr = "MC16a_GSC_factors_29July2017.root"
outputFile = ROOT.TFile.Open(outputStr, "RECREATE")


for key in inputFile.GetListOfKeys():
  print key.GetName()

  if len(key.GetName().split('_')) == 2:
    continue

  outName = key.GetName()
  if "Ntrk" in outName:
    outName = outName.replace("Ntrk","nTrk")
  elif "Wtrk" in outName:
    outName = outName.replace("Wtrk","trackWIDTH")
  elif "Nseg" in outName:
    outName = outName.replace("Nseg","PunchThrough")
    outName = outName.replace('_13','_1')
    if '_19' in outName:
      continue

  hist = inputFile.Get(key.GetName() )
  hist.SetName(outName)
  hist.Write()

outputFile.Close()
inputFile.Close()

