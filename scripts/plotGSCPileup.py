import argparse
import ROOT
import AtlasStyle
import os
import math

def plotGSCPileup():


  inputFileName = args.input.split('_')[-1].replace('.root','')

  pileupRanges = ['10to20', '20to30', '30to40', '40to50', '50to60', '60to70']
  #pileupRanges = ['10to17', '28to40']
  pileupType = 'mu'
  jetType = 'EM'

  if( pileupType == 'mu' ):
    pileupString = '#mu'
  else:
    pileupString = pileupType.capitalize()

  colors = [ROOT.kBlack, ROOT.kOrange, ROOT.kRed, ROOT.kGreen, ROOT.kViolet, ROOT.kBlue ]
  markerStyles = [20, 24, 21, 20, 24, 21]

  # Find the available npv and mu ranges
  inputFile = ROOT.TFile.Open(args.input, "READ")

  #Get full list of pileup ranges (should be second part of name, split by '_'), and get a unique list of these using set function
  unique_pileup_list = list(set([key.GetName().split('_')[1].replace(pileupType,'') for key in inputFile.GetListOfKeys() if "h_"+pileupType in key.GetName() and "_fitted_response_vs_pt_eta" in key.GetName()] ))
  if any( pileupRange not in unique_pileup_list for pileupRange in pileupRanges ):
    print "Error, one or more of the elements in", pileupRanges, "is not available in the file:", unique_pileup_list
    exit(1)

  unique_eta_list = list(set([ '_'.join(key.GetName().split('_')[-2:]) for key in inputFile.GetListOfKeys() if 'h_'+pileupType+pileupRanges[0]+'_fitted_response_vs_pt_eta' in key.GetName()] ))

  oneLine = ROOT.TF1("zl1","1", -10000, 100000)
  oneLine.SetTitle("")
  oneLine.SetLineWidth(1)
  oneLine.SetLineStyle(2)
  oneLine.SetLineColor(ROOT.kBlack)
  zeroLine = ROOT.TF1("zl0","0", -10000, 100000)
  zeroLine.SetTitle("")
  zeroLine.SetLineWidth(1)
  zeroLine.SetLineStyle(2)
  zeroLine.SetLineColor(ROOT.kBlack)

  plotTypes = ['response', 'resolution']
  yAxisRanges = [ [0.9, 1.2], [0, 0.35] ]
  doRatio = [True, True]

  ####### Do inclusive / flavor response comparison ########
  for iPlot, plotType in enumerate(plotTypes):
    print "Processing", pileupType, plotTypes, "plots"
    for etaString in unique_eta_list:
      c1 = ROOT.TCanvas()
      c1.SetTopMargin(0.02)
      c1.SetRightMargin(0.03)
      c1.SetLeftMargin(0.13)
      c1.SetBottomMargin(0.15)
      c1.SetLogx()
      leg = ROOT.TLegend(0.75, 0.55, 0.95, 0.95)
      leg.SetFillStyle(0)

      if(doRatio[iPlot]):
        pad1 = ROOT.TPad("pad1","pad1",0,0.36,1,0.995)
        pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.35)
        pad1.SetLogx()
        pad2.SetLogx()
        pad1.Draw()
        pad2.Draw()
        pad1.SetBottomMargin(0.02)
        pad1.SetTopMargin(0.02)
        pad2.SetTopMargin(0.03)
        pad1.SetRightMargin(0.03)
        pad2.SetRightMargin(0.03)
        pad1.SetLeftMargin(0.13)
        pad2.SetLeftMargin(0.13)
        pad2.SetBottomMargin(.25)
        zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
        zeroLine.SetTitle("")
        zeroLine.SetLineWidth(1)
        zeroLine.SetLineStyle(7)
        zeroLine.SetLineColor(ROOT.kBlack)

      savedHists = [] #Save hists so they're not removed
      ratioHists = []
      for iP, pileupRange in enumerate(pileupRanges):
        if(doRatio[iPlot]):
          pad1.cd()

        savedHist = inputFile.Get( 'h_'+pileupType+pileupRange+'_fitted_'+plotType+'_vs_pt_eta_'+etaString )
        savedHists.append( savedHist )

        ## Configure hist format ##
        savedHist.GetXaxis().SetTitleOffset(0.9)
        savedHist.GetXaxis().SetTitle("p_{T}^{truth}")
        savedHist.GetYaxis().SetTitleOffset(1.2)
        savedHist.GetXaxis().SetMoreLogLabels()
        savedHist.SetMarkerColor(colors[iP])
        savedHist.SetLineColor(colors[iP])
        savedHist.SetMarkerSize(1.)
        savedHist.SetMarkerStyle(markerStyles[iP])
        savedHist.GetYaxis().SetRangeUser(yAxisRanges[iPlot][0], yAxisRanges[iPlot][1] )

        if iP == 0:
          savedHist.DrawCopy("hist")
        else:
          savedHist.DrawCopy("histsame")
          savedHist.Draw("Axis same")

        leg.AddEntry(savedHist, pileupString+' '+pileupRange.replace('to','#rightarrow'), "l")

        ## Now draw ratio ##
        if(doRatio[iPlot]):
          pad2.cd()
          if iP > 0:
            newHist = savedHist.Clone("tmpClone")
            ratioHists.append(newHist)

            newHist.GetYaxis().SetNdivisions(4)
            newHist.GetYaxis().SetLabelSize(0.1)
            newHist.GetXaxis().SetLabelSize(0.12)
            newHist.GetXaxis().SetTitleSize(0.12)
            newHist.GetYaxis().SetTitleSize(0.1)
            newHist.GetYaxis().SetTitleOffset(0.6)
            newHist.SetMarkerSize(0.7)
            newHist.SetMarkerStyle(20)


            iR = 0

            if( plotType == 'resolution'):
              newHist.GetYaxis().SetTitle("sgn(#sigma'-#sigma)#sqrt{#sigma'^{2}-#sigma^{2}}")
              for i in range(1, newHist.GetNbinsX()+1 ):
                value = newHist.GetBinContent(i)**2
                nom = savedHists[iR].GetBinContent(i)**2
                newValue = math.sqrt(math.fabs(value-nom))
                newError = math.sqrt( newHist.GetBinError(i)**2 + savedHists[iR].GetBinError(i)**2)
                if value < nom:
                  newValue *= -1
                newHist.SetBinContent(i, newValue)
                newHist.SetBinError(i, newError)
              newHist.GetYaxis().SetRangeUser(-0.05, 0.25)
            else:
              newHist.GetYaxis().SetTitle("Ratio")
              for i in range(1, newHist.GetNbinsX()+1 ):
                if( savedHists[iR].GetBinContent(i) == 0 ):
                  newValue, newError = 1, 0
                else:
                  newValue = newHist.GetBinContent(i)/savedHists[iR].GetBinContent(i)
                  newError = math.sqrt(newHist.GetBinError(i)**2+savedHists[iR].GetBinError(i)**2)
                newHist.SetBinContent(i, newValue)
                newHist.SetBinError(i, newError)
              newHist.GetYaxis().SetRangeUser(0.94, 1.06)

            if iP == 1:
              newHist.Draw("p")
            else:
              newHist.Draw("psame")

      ## Get correct eta values ##
      c1.cd()
      if(doRatio[iPlot]):
        pad1.Update()
        pad2.Update()
        pad1.cd()
      leg.Draw()
      oneLine.Draw("same")
      if(doRatio[iPlot]):
        pad2.cd()
        if( plotType == 'resolution'):
          zeroLine.Draw("same")
        else:
          oneLine.Draw("same")
        pad1.cd()

      etaLow = int(etaString.split('_')[0])/10.
      etaHigh = int(etaString.split('_')[1])/10.

      if( doRatio[iPlot] ):
        AtlasStyle.ATLAS_LABEL(0.35,0.88, 1.7, "Simulation Internal")
        AtlasStyle.myText(0.35, 0.81, 1.5, "Pythia Dijet #sqrt{s} = 13 TeV")
        AtlasStyle.myText(0.35, 0.74, 1.5, "#it{R}=0.4 anti-#it{k}_{t} "+jetType)
        AtlasStyle.myText(0.35, 0.67, 1.6, str(etaLow)+' < |#eta| < '+str(etaHigh))
        #AtlasStyle.myText(0.16, 0.88, 1.6, str(etaLow)+' < |#eta| < '+str(etaHigh))
      else:
        AtlasStyle.ATLAS_LABEL(0.35,0.92, 1.1, "Simulation Internal")
        AtlasStyle.myText(0.35, 0.85, 1., "Pythia Dijet #sqrt{s} = 13 TeV")
        AtlasStyle.myText(0.35, 0.79, 1., "#it{R}=0.4 anti-#it{k}_{t} "+jetType)
        AtlasStyle.myText(0.35, 0.73, 1.1, str(etaLow)+' < |#eta| < '+str(etaHigh))
        #AtlasStyle.myText(0.16, 0.92, 1.1, str(etaLow)+' < |#eta| < '+str(etaHigh))


      c1.SaveAs(args.outDir+'/pileup_'+pileupType+'_'+plotType+'_'+etaString+'.png')
#      c1.SaveAs(args.outDir+'/pileup_'+pileupType+'_'+plotType+'_'+etaString+'.eps')

      if(doRatio[iPlot]):
        pad1.Clear()
        pad2.Clear()
      c1.Clear()
      leg.Clear()
      c1.cd()




#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--input", dest='input', required=True, default="", help="Single input file")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = os.path.dirname(args.input)+'/GSCPileupPlots/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  plotGSCPileup()
  print "Done plot GSC Pileup"
