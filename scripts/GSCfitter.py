import argparse, os, math, sys
import ROOT, array
sys.path.insert(0, 'JES_ResponseFitter/scripts/')
import JES_BalanceFitter
import AtlasStyle

###Projection on the corrVar and response from the 2D histogram
def produce_1Dhistos( in_resp_vs_x ):

  projXName = in_resp_vs_x.GetName()+"_proj_var"
  h_projX = in_resp_vs_x.ProjectionX(projXName,-1,-1,"e")

  projYName = in_resp_vs_x.GetName()+"_proj_resp"
  h_projY = in_resp_vs_x.ProjectionY(projYName,-1,-1)

  if(h_projY.GetEffectiveEntries() > 10):
    h_projY.Fit("gaus","Q")

  return h_projX, h_projY

### Defining a dynamical binning for the corrVar histogram
def dynamical_binning(h_x, nBinsx):

  lowBins, highBins, binEntries = [], [], []

  ## There is an exception for Tile0 to ensure 1 bin from -0.1 to -0.01
  ## and 1 bin from -0.01 to 0.01, as there are drastic changes in behavior here
  if( args.corrVar == "Tile0" ):
  #if( False ):

    if( args.debug ):
      print "Setting special Tile0 binning for -0.1 to -0.015 and -0.015 to 0.015"

    ## Manually add the 2 bins ##
    lowBins.append(  h_x.FindBin(-0.1) )
    highBins.append( h_x.FindBin(-0.015)-1 )
    lowBins.append(  h_x.FindBin(-0.015) )
    highBins.append( h_x.FindBin(0.015)-1 )
    firstBin = h_x.FindBin(0.015)
    nBinsx -= 2

    ## Start dynamic rebinning with the new firstBin edge
    lastBin = h_x.FindLastBinAbove(0)
    totalEntries = h_x.Integral(firstBin, lastBin)
    lowBins.append(firstBin)

  else:
    firstBin = h_x.FindFirstBinAbove(0)
    lastBin = h_x.FindLastBinAbove(0)
    totalEntries = h_x.Integral(firstBin, lastBin)
    lowBins.append(firstBin)

  iBin = 0
  entries = 0
  for xBin in range(firstBin, lastBin+1):

    entries += h_x.GetBinContent(xBin)

    ## Set this bin edge if we're above the entries, or if the next bin will take us above 1/2 into the next edge
    ## This avoids bins that are too large
    if(  (xBin == lastBin ) or
      (entries >= ( totalEntries*(iBin+1)/nBinsx )) or
      (xBin!=lastBin and entries+h_x.GetBinContent(xBin+1) >= totalEntries*(iBin+1.5)/nBinsx ) ):

      highBins.append( xBin )
      lowBins.append( xBin+1 )
      binEntries.append( h_x.Integral(lowBins[-2], highBins[-1]) )
      iBin += 1

  ##lowBins should be double counted, so remove last element
  del lowBins[-1]

  while (len(lowBins) < nBinsx):
    lowBins.append( highBins[-1]+1 )
    highBins.append( lowBins[-1] )


  return lowBins, highBins, binEntries

### Defining a static binning for the corrVar histogram
def static_binning(h_x, nBinsx):

  binEntries = []

  step = int(h_x.GetNbinsX()/nBinsx)
  lowBins = [i*step+1 for i in range(0,nBinsx)]
  highBins = [i*step for i in range(1,nBinsx+1)]
  highBins[-1] = h_x.GetNbinsX()

  for iBin, lowBin in enumerate(lowBins):
    binEntries.append( h_x.Integral(lowBin, highBins[iBin] ) )

  return lowBins, highBins, binEntries

### Defining a Nseg binning for the corrVar histogram
def Nseg_binning(h_x, nBinsx):

  binEntries = []
  lowBinVals = [0, 2, 10, 20, 30, 40, 50, 60, 80, 100]
  highBinVals = [2, 10, 20, 30, 40, 50, 60, 80, 100, 160]

  lowBins = []
  highBins = []
  for iBin, lowBin in enumerate(lowBinVals):
    lowBins.append( h_x.GetXaxis().FindBin( lowBin ) )
    highBins.append( h_x.GetXaxis().FindBin( highBinVals[iBin] ) -1 )
  for iBin, lowBin in enumerate(lowBins):
    binEntries.append( h_x.Integral(lowBin, highBins[iBin] ) )


  return lowBins, highBins, binEntries

def main():


  ### Setup input options from argparse or input file name
  if args.outDir == "":
    args.outDir = os.path.dirname( args.input )

  ## This is the kinematic variable, typically pt or E ##
  kinematicVar = args.kinematic

  print " Input file = ", args.input
  print " Output dir = ", args.outDir
  print " Correction variable = ", args.corrVar

  ## Create instance of JES_BalanceFitter ##
  NsigmaForFit = 1.6
  myFitter = JES_BalanceFitter.JES_BalanceFitter(NsigmaForFit)
  myFitter.SetGaus()
  #myFitter->SetPoisson() #Poisson was shown to fit better distribution at low pT
  myFitter.SetFitColor(ROOT.kRed)

  ## Prepare output pdf ##
  can = ROOT.TCanvas()
  if( args.doLogY ):
    can.SetLogy()
  can.SetMargin(0.12,0.04,0.12,0.04)
  plotFile_inclusive = args.outDir+'/'+args.outTag+'PlotDump_Fitter_response_'+args.corrVar+'.pdf'
  can.Print(plotFile_inclusive+"[")

  ## Opening input and output files
  inputFile = ROOT.TFile(args.input,"read")
  outputFileName = args.outDir+'/'+args.outTag+'Fitted_'+args.corrVar+'_'+os.path.basename(args.input)
  outputFile = ROOT.TFile(outputFileName,"update")

  ### Get list of eta bins from input file, inclusive & variable must agree! ###
  if (args.corrVar == "Inclusive"):
    ## We use LQ here so as not to fit the Nseg wider eta bins, for which flavor histograms are not included.
    etaRanges = [ '_'.join(key.GetName().split('eta_')[1].split('_')[0:2]) for key in inputFile.GetListOfKeys() if 'h_LQ_response_vs_'+args.kinematic+'_eta_' in key.GetName() ]
  else:
    etaRanges = [ '_'.join(key.GetName().split('_')[-5:-3]) for key in inputFile.GetListOfKeys() if 'h_response_vs_'+args.corrVar in key.GetName() ]
  etaRanges = list(set(etaRanges)) ## Remove duplicates (b/c of pt ranges) ##
  # Sort ranges by integer values #
  etaRanges = [etaRange for (etaLow,etaRange) in sorted(zip([int(etaRange.split('_')[0]) for etaRange in etaRanges ] ,etaRanges))]

  ## Set user min & max eta ranges ##
  if args.etaMin != -1:
    etaRanges = [etaRange for etaRange in etaRanges if int(etaRange.split('_')[0]) >= args.etaMin]
  if args.etaMax != -1:
    etaRanges = [etaRange for etaRange in etaRanges if int(etaRange.split('_')[1]) <= args.etaMax]

  print "Using eta ranges", etaRanges

  ## If fitting pileup hists, grab the npv and mu ranges
  if args.doPileup:
    npvRanges = [ 'npv'+key.GetName().split('npv')[1].split('_')[0] for key in inputFile.GetListOfKeys() if 'npv' in key.GetName() and  '_response_vs_'+args.kinematic+'_eta_0_1' in key.GetName() ]
    muRanges = [ 'mu'+key.GetName().split('mu')[1].split('_')[0] for key in inputFile.GetListOfKeys() if 'mu' in key.GetName() and  '_response_vs_'+args.kinematic+'_eta_0_1' in key.GetName() ]
    print npvRanges
    print muRanges

  ## Prepare to save information from the inclusive fits for the GSC fits ##
  ## We need the eta binning (above), pt binning, and mean fit values

  inclusive_mean = []
  inclusive_ptEdges = []
  inclusive_ptCenters = []

  ## Loop over all eta bins ##
  for iEta, etaString in enumerate(etaRanges):
    print "Running inclusive fits on eta range", etaString.replace('_','->')

    ## Inclusive TH2F of response vs pt for this eta slice ##
    histName = "h_response_vs_"+kinematicVar+"_eta_"+etaString
    h_resp_vs_pt = inputFile.Get( histName )
    if not h_resp_vs_pt:
      print "Error, could not find", histName
      exit(1)

    #### Old methods ####
    ### Get pt edges for this eta slice from the input file ##
    #ptEdges = [ [int(key.GetName().split('_')[-2]), int(key.GetName().split('_')[-1])] for key in inputFile.GetListOfKeys() if 'h_response_vs_'+args.corrVar+'_eta_'+etaString+'_' in key.GetName() ]
    ### Get pt edges for this eta slice from the nominal response histogram ##
    #binEdges = list( h_resp_vs_pt.GetXaxis().GetXbins() )
    ### turn it into integer pairs of bin edges
    #ptEdges = [ [int(binEdges[i]), int(binEdges[i+1])] for i in range(len(binEdges)-1) ]
    #npt = len( ptEdges )
    #print "Got", kinematicVar, "edges", ptEdges

    ## Get pt centers & edges from event distribution ##
    ptEdges = []
    ptCenters = []
    profileName = "p_inclusive_center"+args.kinematic.title()+"_eta_"+etaString
    profile = inputFile.Get( profileName )
    if not profile:
      print "Error, could not fine", profileName
      exit(1)

    ## Choose average if the number of entries is too small ##
    for i in range(1, profile.GetNbinsX()+1):
      ptEdges.append( [ int(profile.GetXaxis().GetBinLowEdge(i)), int(profile.GetXaxis().GetBinUpEdge(i))] )
      measuredCenter = profile.GetBinContent(i)
      ## if the measured center is zero (no data) or has a relative error above 5% (not enough data), then just use the bin center ##
      if( measuredCenter <= 0 or profile.GetBinError(i)/measuredCenter >= 0.05 ):
        ptCenters.append( profile.GetBinCenter(i) )
      else:
        ptCenters.append( measuredCenter )

    # Save ptEdges for GSC fits
    inclusive_ptCenters.append( ptCenters )
    inclusive_ptEdges.append( ptEdges )
    inclusive_mean.append( [] )

    npt = len(ptCenters)

    ## Make 1d fitted response & resolution vs pt histograms for final comparison plots ##
    ptEdgeList = [ptEdge[0] for ptEdge in ptEdges]
    ptEdgeList.append( ptEdges[-1][1] )
    ptEdgeArray = array.array('f', ptEdgeList)

    ## The inclusive fit graphs as a function of pt, inclusive in the corrVar variable ##
    g_response = ROOT.TGraphErrors()
    g_response.SetName( "g_response_eta_"+etaString )
    g_resolution = ROOT.TGraphErrors()
    g_resolution.SetName( "g_resolution_eta_"+etaString )

    if args.corrVar == "Nseg":
      responseTypes = []
    elif args.doPileup:
      responseTypes = npvRanges+muRanges
    else:
      responseTypes = ["All","LQ","gluon","b","c"]

    responseHists1D_out = []
    resolutionHists1D_out = []
    responseHists2D_in = []
    for responseType in responseTypes:
      if responseType == "All":
        responseString = ""
      else:
        responseString = responseType+'_'

      thisInHistName = histName.replace('response',responseString+'response')
      thisInputHist2D = inputFile.Get( thisInHistName )
      responseHists2D_in.append( thisInputHist2D )
      if responseType == "All":
        thisInHistName = thisInHistName.replace('response','All_response')

      h_response = ROOT.TH1F( thisInHistName.replace('response','fitted_response'), responseType+" response vs pt", npt, ptEdgeArray)
      h_response.GetXaxis().SetTitle( thisInputHist2D.GetXaxis().GetTitle() )
      if kinematicVar== "E":
        h_response.GetYaxis().SetTitle( "Response (E^{reco}/E^{truth})" )
      else:
        h_response.GetYaxis().SetTitle( "Response (p_{T}^{reco}/p_{T}^{truth})" )
      responseHists1D_out.append( h_response )

      h_resolution = ROOT.TH1F( thisInHistName.replace('response','fitted_resolution'), responseType+" resolution vs pt", npt, ptEdgeArray)
      h_resolution.GetXaxis().SetTitle( thisInputHist2D.GetXaxis().GetTitle() )
      h_resolution.GetYaxis().SetTitle( "Resolution (#sigma_{R}/R)" )
      resolutionHists1D_out.append( h_resolution )

    ######## Run over each pt slice ########
    for iPt in range(0, npt):

      ## Get pt info ##
      ptLow  = ptEdges[iPt][0]
      ptHigh = ptEdges[iPt][1]
      ptCenter = ptCenters[iPt]
      ptDiff  = (ptHigh-ptLow)/2.

      ## Get pt/E projection of x-var inclusive TH2F ##
      projName = 'h_projections_eta_'+etaString+'_'+kinematicVar+'_'+str(ptLow)+'_'+str(ptHigh)
      inclLowBin = h_resp_vs_pt.GetXaxis().FindBin( ptLow )
      ## ProjectionY is inclusive, and FindBin is not!  So subtract by a tiny number to get the correct bin ##
      inclHighBin = h_resp_vs_pt.GetXaxis().FindBin( ptHigh-0.1 )
      h_resp_projection = h_resp_vs_pt.ProjectionY(projName, inclLowBin, inclHighBin )

      ## Fit the inclusive distribution for this eta / pt slice ##
      ## This will give the mean response (inclusive over the correction variable),
      ## which will be used to normalize the various correction-dependent fits, ensuring the JES is unchanged ##
      mean = 1.
      meanStats = h_resp_projection.GetEffectiveEntries()
      ## Only run inclusive fits if there are at least 100 events ##
      if( h_resp_projection.GetEffectiveEntries() > 100 ):

        ## Rebin distributions with less events
        if( h_resp_projection.GetEffectiveEntries() < 3500 ):
          h_resp_projection.Rebin(2)

        ## Get fit edges from histogram content ##
        maxContent = h_resp_projection.GetBinContent(h_resp_projection.GetMaximumBin())

        highThreshold = 1/4.
        lowThreshold = 1/4.
        if( ptLow < 25 ):
          lowThreshold = 0.7
        elif( ptLow < 40 ):
          lowThreshold = 0.5

        fitLow = h_resp_projection.GetXaxis().GetBinLowEdge( h_resp_projection.FindFirstBinAbove(maxContent*lowThreshold)-1 )
        fitHigh = h_resp_projection.GetXaxis().GetBinUpEdge( h_resp_projection.FindLastBinAbove(maxContent*highThreshold)+1 )

        myFitter.Fit(h_resp_projection, fitLow, fitHigh )

        ## Get fit variables ##
        mean = myFitter.GetMean()
        mean_err = myFitter.GetMeanError()
        sigma = myFitter.GetSigma()
        sigma_err = myFitter.GetSigmaError()
        resolution_err = math.sqrt( (sigma_err/mean)**2 + (sigma*mean_err/(mean**2) )**2 )

        ## Draw gaussian distribution and fits ##
        myFitter.DrawFitAndHisto()
        if( args.doLogY ):
          myFitter.GetHisto().SetMaximum(  myFitter.GetHisto().GetMaximum()*100)
          myFitter.GetHisto().SetMinimum( myFitter.GetHisto().GetMaximum()/100000)
        AtlasStyle.myText( 0.56, 0.88, 0.8, kinematicVar+": {:.0f} to {:.0f}".format( ptLow, ptHigh ) )
        AtlasStyle.myText( 0.56, 0.84, 0.8, h_resp_projection.GetName() )
        AtlasStyle.myText( 0.56, 0.80, 0.8, "Effective entries: {:}".format(int(h_resp_projection.GetEffectiveEntries())) )
        AtlasStyle.myText( 0.56, 0.76, 0.8, "Fit mean: {:.4f} error: {:.4f}".format(mean, mean_err) )
        lowFit, highFit = ROOT.Double(0), ROOT.Double(0)
        myFitter.GetFit().GetRange(lowFit, highFit)
        AtlasStyle.myText( 0.56, 0.72, 0.8, "Fit range: {:.3f} to {:.3f}".format(lowFit, highFit) )
        AtlasStyle.myText( 0.56, 0.68, 0.8, "Reduced chi2: {:.2f}".format(myFitter.GetChi2Ndof()) )
        AtlasStyle.myText( 0.56, 0.64, 0.8, "Hist mean: {:.4f}".format(h_resp_projection.GetMean()) )

        if( myFitter.GetChi2Ndof() > 10 ):
          AtlasStyle.myBlueText( 0.55, 0.4, 4, "BAD FIT")

        can.SaveAs(plotFile_inclusive)

        ## Set point for the inclusive response vs pt plot ##
        g_response.SetPoint(iPt, ptCenter, mean )
        g_response.SetPointError(iPt, ptDiff, mean_err )
        g_resolution.SetPoint( iPt, ptCenter, sigma/mean )
        g_resolution.SetPointError( iPt, ptDiff, resolution_err )

        if( mean_err <= 0 or resolution_err <= 0):
          print("Error, mean_err is",mean_err,"and resolution_err is",resolution_err)
          exit(1)

        ##  Now fit the various response and resolution histograms ##
        for iR, responseType in enumerate(responseTypes):
          projName = 'h_'+responseType+'_fitted_projections_eta_'+etaString+'_'+kinematicVar+'_'+str(ptLow)+'_'+str(ptHigh)
          h_flavor_resp_projection = responseHists2D_in[iR].ProjectionY(projName, inclLowBin, inclHighBin )

          if( h_flavor_resp_projection.GetEffectiveEntries() > 100 ):

            ## Rebin distributions with less events
            if( h_flavor_resp_projection.GetEffectiveEntries() < 3500 ):
              h_flavor_resp_projection.Rebin(2)

            ## Get fit edges from histogram content ##
            maxContent = h_flavor_resp_projection.GetBinContent(h_flavor_resp_projection.GetMaximumBin())

            highThreshold = 1/4.
            lowThreshold = 1/4.
            if( ptLow < 25 ):
              lowThreshold = 0.7
            elif( ptLow < 40 ):
              lowThreshold = 0.5

            fitLow = h_flavor_resp_projection.GetXaxis().GetBinLowEdge( h_flavor_resp_projection.FindFirstBinAbove(maxContent*lowThreshold)-1 )
            fitHigh = h_flavor_resp_projection.GetXaxis().GetBinUpEdge( h_flavor_resp_projection.FindLastBinAbove(maxContent*highThreshold)+1 )

            myFitter.Fit(h_flavor_resp_projection, fitLow, fitHigh )

            ## Get fit variables ##
            flavor_mean = myFitter.GetMean()
            flavor_mean_err = myFitter.GetMeanError()
            flavor_sigma = myFitter.GetSigma()
            flavor_sigma_err = myFitter.GetSigmaError()
            flavor_resolution_err = math.sqrt( (flavor_sigma_err/flavor_mean)**2 + (flavor_sigma*flavor_mean_err/(flavor_mean**2) )**2 )

            responseHists1D_out[iR].SetBinContent(iPt+1, flavor_mean)
            responseHists1D_out[iR].SetBinError(iPt+1, flavor_mean_err)
            resolutionHists1D_out[iR].SetBinContent(iPt+1, flavor_sigma/flavor_mean)
            resolutionHists1D_out[iR].SetBinError(iPt+1, flavor_resolution_err)

      ## Save inclusive mean for GSC fits ##
      inclusive_mean[iEta].append( mean )

    ## Write out inclusive TGraphs & histograms ##
    g_response.Write()
    g_resolution.Write()
    for iR, responseType in enumerate(responseTypes):
      responseHists1D_out[iR].Write()
      resolutionHists1D_out[iR].Write()

  can.Print(plotFile_inclusive+"]")

##################################################################################################################3
################## Now do the GSC fits over the variable ##########################
  if (not args.corrVar == "Inclusive"):
    plotFile = args.outDir+'/'+args.outTag+'PlotDump_Fitter_Var_'+args.corrVar+'.pdf'
    can.Print(plotFile+"[")


    for iEta, etaString in enumerate(etaRanges):
      print "Running GSC fits on eta range", etaString.replace('_','->')

      ptEdges = inclusive_ptEdges[iEta]
      ptCenters = inclusive_ptCenters[iEta]
      npt = len( ptEdges )

      #3D map of response vs corrVar vs pt
      g_2D = ROOT.TGraph2D(args.nx*npt)
      g_2D.SetName( "g_2D_"+kinematicVar+"_eta_"+etaString )

      #3D map of response vs x vs pt reco
      g_2D_ptreco  = ROOT.TGraph2D(args.nx*npt)
      g_2D_ptreco.SetName( "g_2D_"+kinematicVar+"reco_eta_"+etaString )

      #3D map of corrected response vs x vs pt reco
      g_2D_ptrecoCorrected  = ROOT.TGraph2D(args.nx*npt)
      g_2D_ptrecoCorrected.SetName( "g_2D_"+kinematicVar+"recoCorrected_eta_"+etaString )

      ######## Run over each pt slice ########
      for iPt in range(0, npt):

        ## Retrieve inclusive mean ##
        mean = inclusive_mean[iEta][iPt]

        ## Get pt info ##
        ptLow  = ptEdges[iPt][0]
        ptHigh = ptEdges[iPt][1]
        ptCenter = ptCenters[iPt]

        ###### Prepare fitting of each corrVar slices ######

        ## Projections in Y for each slides of variable x ##
        h_resp_vs_pt_vs_corrVar = []
        yBin_center, yBin_err, yBinCorr_center, yBinCorr_err = [], [], [], []

        if (mean == 1.):
          print "Response is exactly one for eta:", etaString.replace('_','->'), "and ", kinematicVar, ptLow, "->", ptHigh, ". Not enough stats: ", meanStats

        ## Get 2D histogram of response vs variable ##
        inputHistName = 'h_response_vs_'+args.corrVar+"_eta_"+etaString+"_"+kinematicVar+"_"+str(ptLow)+"_"+str(ptHigh)
        if (args.debug):
          print "Getting input histogram ", inputHistName
        h_resp_vs_corrVar = inputFile.Get(inputHistName)

        ## Get corrVar and response projections ##
        h_corrVar, h_resp = produce_1Dhistos(h_resp_vs_corrVar)

        ## Choose binning ##
        # N.B. (not from Jeff) ->  No funciona bien, por mas que corra el numero de entradas es cero y luego cuando el numero de entradas es =! de cero salta el break
        if( args.corrVar == "Nseg" ):
          lowBin, highBin, entriesN = Nseg_binning(h_corrVar, args.nx)
        elif (h_corrVar.GetEffectiveEntries() < 25):
          lowBin, highBin, entriesN = static_binning(h_corrVar, args.nx)
        else:
          lowBin, highBin, entriesN = dynamical_binning(h_corrVar, args.nx)



        ## Get weighted center and edges for TGraph for each region of the corrVar (e.g. Tile0) ##
        xBin_center, xBin_low, xBin_high = [], [], []
        for iX in range(0, args.nx):
          entries = 0
          weightedCenter = 0

          for iBin in range(lowBin[iX], highBin[iX]+1):
            entries += h_corrVar.GetBinContent(iBin)
            weightedCenter += h_corrVar.GetBinContent(iBin)*h_corrVar.GetBinCenter(iBin)

          if (h_corrVar.GetEffectiveEntries() > 25 and entries > 0):
            weightedCenter /= entries
          else:
            weightedCenter = (h_corrVar.GetBinCenter(highBin[iX])+h_corrVar.GetBinCenter(lowBin[iX]))/2


          xBin_center.append( weightedCenter )
          xBin_low.append( weightedCenter - h_corrVar.GetXaxis().GetBinLowEdge(lowBin[iX]) )
          xBin_high.append( h_corrVar.GetXaxis().GetBinUpEdge(highBin[iX]) - weightedCenter )



        ## Loop over corrVar bins ##
        for iX in range(0, args.nx):

          name = h_resp_vs_corrVar.GetName()+"_x_"+str(iX)
          h = h_resp_vs_corrVar.ProjectionY(name,lowBin[iX],highBin[iX])


          if( h.GetEffectiveEntries() < 100 ):
            yBin_center.append( 0 )
            yBin_err.append( 0 )
            yBinCorr_center.append( 0 )
            yBinCorr_err.append( 0 )

          else:

            if( h.GetEffectiveEntries() < 3500 ):
              h.Rebin(2)

            ## Set fit edges from histogram content ##
            maxContent = h.GetBinContent(h.GetMaximumBin())

            highThreshold = 1/4.
            lowThreshold = 1/4.
            if( ptLow < 25 ):
              lowThreshold = 0.6
            elif( ptLow < 40 ):
              lowThreshold = 0.5

            fitLow = h.GetXaxis().GetBinLowEdge( h.FindFirstBinAbove(maxContent*lowThreshold)-1 )
            fitHigh = h.GetXaxis().GetBinUpEdge( h.FindLastBinAbove(maxContent*highThreshold)+1 )

            myFitter.Fit(h, fitLow, fitHigh )

            #XX ## refit if mean is less than zero, slowly increasing fit range ##
            #XX width = 0.2
            #XX histCenter = h.GetMean()
            #XX while (myFitter.GetMean() < 0):
            #XX   myFitter.Fit(h, histCenter-width, histCenter+width)
            #XX   width += 0.05

            ## Save fit information ##
            yBin_center.append( myFitter.GetMean() )
            if( myFitter.GetMeanError() == 0):
              print "ERROR, a fit error was zero for", etaString, "and ", kinematicVar, ptLow, "->", ptHigh, ". Setting error to 1"
              yBin_err.append( 1.0 )
            else:
              yBin_err.append( myFitter.GetMeanError() )

            yBinCorr_center.append( yBin_center[-1] / mean )
            yBinCorr_err.append( yBin_err[-1] / mean ) ##Previously was not scaled by mean...


            ## Draw hist and fit, and save to TCanvas ##
            myFitter.DrawFitAndHisto()
            if( args.doLogY ):
              myFitter.GetHisto().SetMaximum(  myFitter.GetHisto().GetMaximum()*100)
              myFitter.GetHisto().SetMinimum( myFitter.GetHisto().GetMaximum()/100000)

            AtlasStyle.myText( 0.56, 0.84, 0.8, h.GetName() )
            AtlasStyle.myText( 0.56, 0.88, 0.8, "x-var: {:.3f} to {:.3f}".format(h_resp_vs_corrVar.GetXaxis().GetBinLowEdge(lowBin[iX]),h_resp_vs_corrVar.GetXaxis().GetBinUpEdge(highBin[iX])) )
            AtlasStyle.myText( 0.56, 0.80, 0.8, "Effective entries: {:}".format(int(h.GetEffectiveEntries())) )
            AtlasStyle.myText( 0.56, 0.76, 0.8, "Fit mean: {:.4f} error: {:.4f}".format(yBin_center[-1], yBin_err[-1]) )
            AtlasStyle.myText( 0.56, 0.72, 0.8, "Corr mean: {:.4f} error: {:.4f}".format(yBinCorr_center[-1], yBinCorr_err[-1]) )
            lowFit, highFit = ROOT.Double(0), ROOT.Double(0)
            myFitter.GetFit().GetRange(lowFit, highFit)
            AtlasStyle.myText( 0.56, 0.68, 0.8, "Fit range: {:.3f} to {:.3f}".format(lowFit, highFit) )
            AtlasStyle.myText( 0.56, 0.64, 0.8, "Reduced chi2: {:.2f}".format(myFitter.GetChi2Ndof()) )
            AtlasStyle.myText( 0.56, 0.60, 0.8, "Hist mean: {:.4f}".format(h.GetMean()) )

            if( myFitter.GetChi2Ndof() > 10 ):
              AtlasStyle.myBlueText( 0.55, 0.4, 4, "BAD FIT")

            can.SaveAs(plotFile)

          ## Add this histogram with the fit to the output list ##
          h_resp_vs_pt_vs_corrVar.append(h)

          if(args.debug):
            print "After fit Eta = ", etaString , ", iPt  = ", iPt ,", iX = ",iX,", Response = ", yBin_center[iX], ", respCorr = ", mean, ", Corrected = ", yBinCorr_center[iX]

          ## Set y-values in overall TGraphs for this xBin ##
          g_2D.SetPoint(                iX+iPt*args.nx, ptCenter,                 xBin_center[iX], yBin_center[iX])
          g_2D_ptreco.SetPoint(         iX+iPt*args.nx, ptCenter*yBin_center[iX], xBin_center[iX], yBin_center[iX]) # Numerical Inversion to reco level
          g_2D_ptrecoCorrected.SetPoint(iX+iPt*args.nx, ptCenter*yBin_center[iX], xBin_center[iX], yBin_center[iX]/mean) # Corrected for the mean response

        # End loop over xbins of the corrVar here

        #Making graph response vs corrVar variable
        g_resp_vs_corrVar = ROOT.TGraphAsymmErrors(args.nx)
        g_resp_vs_corrVar.SetName( "g_resp_vs_"+args.corrVar+"_eta_"+etaString+'_'+kinematicVar+'_'+str(ptLow)+'_'+str(ptHigh) )
        g_resp_vs_corrVar.GetYaxis().SetTitle("Fitted "+h_resp_vs_corrVar.GetYaxis().GetTitle() )
        g_resp_vs_corrVar.GetXaxis().SetTitle( h_resp_vs_corrVar.GetXaxis().GetTitle() )
        for iX, xval in enumerate(xBin_center):
          g_resp_vs_corrVar.SetPoint(iX, ROOT.Double(xval), ROOT.Double(yBin_center[iX]) )
          g_resp_vs_corrVar.SetPointError(iX, ROOT.Double(xBin_low[iX]), ROOT.Double(xBin_high[iX]), ROOT.Double(yBin_err[iX]), ROOT.Double(yBin_err[iX]) )

        #Making graph response, corrected for mean, vs corrVar variable
        g_respCorrected_vs_corrVar = ROOT.TGraphAsymmErrors(args.nx)
        g_respCorrected_vs_corrVar.SetName( "g_respCorrected_vs_"+args.corrVar+"_eta_"+etaString+'_'+kinematicVar+'_'+str(ptLow)+"_"+str(ptHigh) )
        g_respCorrected_vs_corrVar.GetYaxis().SetTitle("Fitted "+h_resp_vs_corrVar.GetYaxis().GetTitle() )
        g_respCorrected_vs_corrVar.GetXaxis().SetTitle( h_resp_vs_corrVar.GetXaxis().GetTitle() )
        for iX, xval in enumerate(xBin_center):
          g_respCorrected_vs_corrVar.SetPoint(iX, ROOT.Double(xval), ROOT.Double(yBinCorr_center[iX]) )
          g_respCorrected_vs_corrVar.SetPointError(iX, ROOT.Double(xBin_low[iX]), ROOT.Double(xBin_high[iX]), ROOT.Double(yBinCorr_err[iX]), ROOT.Double(yBinCorr_err[iX]) )


        ## Write hists to output file ##
        g_resp_vs_corrVar.Write(g_resp_vs_corrVar.GetName(), ROOT.TObject.kOverwrite)
        g_respCorrected_vs_corrVar.Write(g_respCorrected_vs_corrVar.GetName(), ROOT.TObject.kOverwrite)
        h_resp_vs_corrVar.Write(h_resp_vs_corrVar.GetName(), ROOT.TObject.kOverwrite)
        ## This is the shape of the correction variable
        h_corrVar.Write()

      #End loop over ptbins

      ## Write out 2D TGraphs of responses ##
      g_2D.Write()
      g_2D_ptreco.Write()
      g_2D_ptrecoCorrected.Write()


    #End of loop over eta bins

    ## Save Canvas and close files ##
    can.Print(plotFile+"]")

  ## End GSC-specific fits ##

  ## Save all pt bin centers ##
  for key in outputFile.GetListOfKeys():
    if "p_inclusive_center" in key.GetName():
      hist = outputFile.Get( key.GetName() )
      hist.Write()

  outputFile.Close()
  inputFile.Close()


if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--doLogY", dest='doLogY', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--doPileup", dest='doPileup', action='store_true', default=False, help="Do pileup-dependent fits")
  parser.add_argument("--debug", dest='debug', action='store_true', default=False, help="More verbose for debugging.")
  parser.add_argument("--input", dest='input', default="", required=True, help="Input file name.")
  parser.add_argument("--correction", dest='corrVar', default="", required=True, help="Correction variable for the x-axis.  Example are Tile0 and nTrk.  Set to Inclusive to skip variable fitting.")
  parser.add_argument("--kinematic", dest='kinematic', default="pt", help="Kinematic variable, generally pt. Set to E for Nseg.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory.")
  parser.add_argument("--outTag", dest='outTag', default="", help="Additional string for output file name.")

  parser.add_argument("--etaMin", dest='etaMin', type=int, default=-1, help="Minimum eta to run over.  If not specified, will take the minimum available in the input file.")
  parser.add_argument("--etaMax", dest='etaMax', type=int, default=-1, help="Maximum eta to run over.  If not specified, will take the maximum available in the input file.")
  parser.add_argument("--numXbins", dest='nx', type=int, default=10, help="Number of bins to use for the x variable (e.g. Tile0).")
  args = parser.parse_args()

  ### TODO don't override these if options are given ###
  if( args.corrVar == "Tile0"):
    args.nx = 11
  elif( args.corrVar == "Nseg"):
    args.kinematic = "E"

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  if( args.outTag != "" ):
    args.outTag = args.outTag+'_'

  main()

