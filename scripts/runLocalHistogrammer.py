#!/usr/bin/python

##############################################################
# runLocalHistogrammer.py                                    #
##############################################################
# Run several instances of GSC/Root/HistogramTree.cxx in
# parallel (up to --ncores jobs at once).  Each job runs on
# a single JZ*W TTree, and outputs weighted histograms.
# The output should be combined into a single file and used
# for GSC fitting.
##############################################################
# Jeff.Dandoy                                                #
##############################################################

import os, math, sys, glob, subprocess, time, shutil
import argparse


## The script options, with details given by the help field
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='fileDir', default="./",
     help="Path to the directory containing the input TTrees")
parser.add_argument("--outDir", dest='outDir', default="",
     help="Path to the output directory of histograms")
parser.add_argument("--inTag", dest='inputTag', default="",
     help="Input tag for choosing files.  Only files containing the specified tag will be used.")
parser.add_argument("--outTag", dest='outputTag', default="NewStudy",
     help="Output naming tag for the histogram root files")
parser.add_argument("--outName", dest='outName', default="Output.root",
     help="Output name for the combined histogram root file")
parser.add_argument("--ncores", dest='ncores', default=4,
     type=int, help="Number of parallel jobs ")
parser.add_argument("--config", dest='config', default="GSC/data/config_Hist_GSC.py",
     help="HistogramTree config file")
args = parser.parse_args()



def main():
  test = False # does not run the jobs, but outputs the individual jobs commands to screen
  if not test:

    ## Make sure localGSCJobs is empty by deleting it, then remake it
    if os.path.exists("gridOutput/localGSCJobs"):
      shutil.rmtree("gridOutput/localGSCJobs")
    os.makedirs('gridOutput/localGSCJobs')

  ## output tag is partially specified by user, and partially the time stamp
  args.logTag = args.outputTag + time.strftime("_%Y%m%d")

  if args.fileDir.endswith('/'):
    args.fileDir = args.fileDir[:-1]

  if args.outDir == "":
    args.outDir = args.fileDir+'/../'+args.outputTag

  ## Gather the files to use as input
  files = glob.glob(args.fileDir+'/*'+args.inputTag+'*.root')
  print files

  ## Create log files of output
  if not os.path.exists('logs/'+args.logTag+'/'):
    os.makedirs('logs/'+args.logTag+'/')

  outHistName = 'hist-'+args.fileDir.split('/')[-1]+'.root'

  pids, logFiles = [], []
  ## Submit each histogramming job ##
  for file in files:

    ## If # jobs > ncores, wait until a job finishes
    if len(pids) >= args.ncores:
      wait_completion(pids, logFiles)

    ## Build the command string for the next jobs
    fileTag = os.path.basename(file)[:-5]+'_'+args.outputTag #remove path and .root
    logFile='logs/'+args.logTag+'/HistogramTree_{0}'.format(fileTag)+'.log'
    submit_dir = 'gridOutput/localGSCJobs/'+fileTag

    command = './xAODAnaHelpers/scripts/xAH_run.py -f --treeName GSC_tree --files '+file+' --submitDir '+submit_dir+' --config '+args.config+' direct'
    print command

    if not test:
      res = submit_local_job(command, logFile)
      pids.append(res[0])
      logFiles.append(res[1])

  ## Wait for all remaining jobs to finish
  wait_all(pids, logFiles)
  for f in logFiles:
    f.close()

  ## Now collect output and put in outDir ##
  if not test:
    if not os.path.exists(args.outDir):
      os.makedirs(args.outDir)

    print 'Moving files to '+args.outDir
    outDirs = glob.glob('gridOutput/localGSCJobs/*')
    print 'Hadding files to '+args.outDir+'/'+args.outName
    ## Output is named hist-output.root by default; Rename and move these files ##
    if not args.outName.endswith('.root'):
      args.outName += '.root'
    os.system('hadd -f '+args.outDir+'/'+args.outName+' gridOutput/localGSCJobs/*/'+outHistName)
    outDirs = glob.glob('gridOutput/localGSCJobs/*')
    for outDir in outDirs:
      shutil.rmtree(outDir)


## This function submits a single job in the background, and returns the pid and logfile
def submit_local_job(exec_sequence, logfilename):
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.5)  #Wait to prevent opening / closing of several files

  return pid, output_f

## This functions waits for any one of the submitted jobs to finish
def wait_completion(pids, logFiles):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(3) # wait before retrying

## This functions waits for all of the remaining jobs to finish
def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"

if __name__ == "__main__":
    main()
