#!/usr/bin/env python

###########################################################
# reweightTrees.py                                        #
# A short script to reweight TTrees by their initial      #
# event number.  Allows for directly combining TTrees.    #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import glob, array, argparse, math
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--file", dest='file', default="", help="Input file")
parser.add_argument("--treeName", dest='treeName', default="nominal", help="Name of trees to be reweighted")
parser.add_argument("--outName", dest='outName', default="", help="Name of output ttree")

args = parser.parse_args()


from ROOT import *

def reweightTrees():

  print "Reweighting file ", args.file

  inFile = TFile.Open(args.file, "READ")
  if( not inFile):
    print "Error, could not get input file, exiting!"
    exit(1)
  
  metaHist = inFile.Get("MetaData_EventCount")
  if( not metaHist):
    print "Error, could not get meta data hist, exiting!"
    exit(1)
  
  tree = inFile.Get( args.treeName )
  if( not tree ):
    print "Warning, cuold not get input tree, it may not exist as there are no events, returning!"
    return

  
  scaleFactor = 1./metaHist.GetBinContent(1)

  print scaleFactor
  print tree.GetName()

  outFileName = args.file.replace(".root","")+"_"+args.treeName+"_reweighted.root"
  outFile = TFile.Open(outFileName, "RECREATE")

  #newTree = tree.CloneTree(0)
  #newTree.SetName(args.outName)

  tree.SetBranchStatus("*", 0)

  weight = array.array('f', [0])
  tree.SetBranchStatus("weight", 1)
  tree.SetBranchAddress("weight", weight)
  rho = array.array('f', [0])
  tree.SetBranchStatus("rho", 1)
  tree.SetBranchAddress("rho", rho)
  NPV = array.array('I', [0])
  tree.SetBranchStatus("NPV", 1)
  tree.SetBranchAddress("NPV", NPV)
  averageInteractionsPerCrossing = array.array('f', [0])
  tree.SetBranchStatus("averageInteractionsPerCrossing", 1)
  tree.SetBranchAddress("averageInteractionsPerCrossing", averageInteractionsPerCrossing)

  jet_true_pt = std.vector('float')() 
  tree.SetBranchStatus("jet_true_pt", 1)
  tree.SetBranchAddress("jet_true_pt", jet_true_pt)
  jet_true_eta = std.vector('float')() 
  tree.SetBranchStatus("jet_true_eta", 1)
  tree.SetBranchAddress("jet_true_eta", jet_true_eta)
  jet_true_phi = std.vector('float')() 
  tree.SetBranchStatus("jet_true_phi", 1)
  tree.SetBranchAddress("jet_true_phi", jet_true_phi)
  jet_true_e = std.vector('float')() 
  tree.SetBranchStatus("jet_true_e", 1)
  tree.SetBranchAddress("jet_true_e", jet_true_e)
  jet_PileupPt = std.vector('float')() 
  tree.SetBranchStatus("jet_PileupPt", 1)
  tree.SetBranchAddress("jet_PileupPt", jet_PileupPt)
  jet_PileupEta = std.vector('float')() 
  tree.SetBranchStatus("jet_PileupEta", 1)
  tree.SetBranchAddress("jet_PileupEta", jet_PileupEta)
  jet_PileupPhi = std.vector('float')() 
  tree.SetBranchStatus("jet_PileupPhi", 1)
  tree.SetBranchAddress("jet_PileupPhi", jet_PileupPhi)
  jet_PileupE = std.vector('float')() 
  tree.SetBranchStatus("jet_PileupE", 1)
  tree.SetBranchAddress("jet_PileupE", jet_PileupE)
  jet_DetEta = std.vector('float')() 
  tree.SetBranchStatus("jet_DetEta", 1)
  tree.SetBranchAddress("jet_DetEta", jet_DetEta)

  newTree = TTree(args.outName, "newTree")
  newTree.Branch("EW", weight, "EW/F")
  newTree.Branch("mu", averageInteractionsPerCrossing, "mu/F")
  newTree.Branch("npv", NPV, "npv/I")
  newTree.Branch("RHO", rho, "RHO/F")
  newTree.Branch("tpt", jet_true_pt)
  newTree.Branch("teta", jet_true_eta)
  newTree.Branch("tphi", jet_true_phi)
  newTree.Branch("te", jet_true_e)
  newTree.Branch("pt",  jet_PileupPt)
  newTree.Branch("eta", jet_PileupEta)
  newTree.Branch("phi", jet_PileupPhi)
  newTree.Branch("e",   jet_PileupE)
  newTree.Branch("deteta",   jet_DetEta)

  jet_PileupM = std.vector('float')() 
  newTree.Branch("m", jet_PileupM)
  jet_true_m = std.vector('float')() 
  newTree.Branch("tm", jet_true_m)

  i = 0
  print tree.GetEntries(), " entries !!"
  while tree.GetEntry(i):
    i += 1

    if(i%10000==0): print "Event",i
    
    if jet_true_pt.size() <= 0:
      continue

    TLV = TLorentzVector()
    for iJ in range(0, jet_true_pt.size()):
      TLV.SetPtEtaPhiE(jet_PileupPt.at(iJ), jet_PileupEta.at(iJ), jet_PileupPhi.at(iJ), jet_PileupE.at(iJ))
      jet_PileupM.push_back( TLV.M() )
      TLV.SetPtEtaPhiE(jet_true_pt.at(iJ), jet_true_eta.at(iJ), jet_true_phi.at(iJ), jet_true_e.at(iJ) )
      jet_true_m.push_back( TLV.M() )

    weight[0] = weight[0] * scaleFactor
    newTree.Fill()

    jet_true_m.clear()
    jet_PileupM.clear()

# newTree.Write("", TObject.kOverwrite)
  outFile.Write()


if __name__ == "__main__":
  reweightTrees()
  print "Finished reweightTrees()"
