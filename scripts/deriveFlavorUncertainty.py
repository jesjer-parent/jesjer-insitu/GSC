import argparse
import ROOT
import AtlasStyle
import os, math, array

def deriveFlavorUncertainty():

  doRatio = True

  inputFiles = [
                 "Fitted_Inclusive_Pythia.root",
                 "Fitted_Inclusive_Herwig.root"
                 ]
  corrTypes = ["Pythia8", "Herwig"]
  fileColors = [ROOT.kBlack, ROOT.kRed-3]
  markerStyle = [20, 24]

  inputFiles = [args.path +'/' + inputFile for inputFile in inputFiles]

  flavorTypes = ["LQ","gluon","b", "c"]
  flavorString = ["light-quarks","gluons","b-quarks", "c-quarks"]

  #3D file : responseType : eta
  respHists = []
  etaList = set([])

  ## Gather all input histograms from the various files ##
  for iF, inputFile in enumerate(inputFiles):
    inFile = ROOT.TFile.Open(inputFile, "READ")
    print(inputFile)
    keys = inFile.GetListOfKeys()
    respHists.append( {} )

    for flavType in flavorTypes:

      respHists[iF][flavType] = {}

      respHistNames = [key.GetName() for key in keys if flavType+"_fitted_response" in key.GetName() ]

      for respHistName in respHistNames:
        etaString = '_'.join(respHistName.split('_')[-2:])
        etaList.add( etaString )

        hist = inFile.Get(respHistName)
        hist.SetDirectory(0)
        respHists[iF][flavType][etaString] = hist

  etaList = list(etaList)
  ## Get actual eta edges from etaList strings
  etaEdgeList_lowEdge = [float(etaRange.split('_')[0])/10.  for etaRange in etaList]
  etaEdgeList_highEdge = [float(etaRange.split('_')[1])/10.  for etaRange in etaList]
  etaEdgeList = list(set(etaEdgeList_lowEdge+etaEdgeList_highEdge)) #Get only unique edges
  etaEdgeList.sort()

  print("etaEdgeList is", etaEdgeList)

  etaCopy = etaEdgeList[:]
  etaCopy = etaCopy[1:] #Remove zero entry
  etaCopy.reverse()
  etaCopy = [-eta for eta in etaCopy] #Make negative
  mirroredEtaList = etaCopy+etaEdgeList

  # Get ptEdgeList using an example histogram (all hist will have same binning)
  ptEdgeList = []
  ptEdgeList.append( respHists[0]["LQ"][etaList[0]].GetXaxis().GetBinLowEdge(1) )
  for iBin in range(1, respHists[0]["LQ"][etaList[0]].GetNbinsX()+1):
    ptEdgeList.append( respHists[0]["LQ"][etaList[0]].GetXaxis().GetBinUpEdge(iBin) )

  print("ptEdgeList is", ptEdgeList)
  print("mirrored is ", mirroredEtaList)


  ####### Get relevant 1D histograms and add them to the 2D hist ########
  gluonResponsePythia2D = ROOT.TH2D("gluonResponsePythia_"+args.jetType, "gluonResponsePythia_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )
  lqResponsePythia2D = ROOT.TH2D("lqResponsePythia_"+args.jetType, "lqResponsePythia_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )
  gluonResponseDiff2D = ROOT.TH2D("gluonResponseDiff_"+args.jetType, "gluonResponseDiff_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )
  bResponseDiff2D = ROOT.TH2D("bResponseDiff_"+args.jetType, "bResponseDiff_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )
  lqResponseDiff2D = ROOT.TH2D("lqResponseDiff_"+args.jetType, "lqResponseDiff_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )
  cResponseDiff2D = ROOT.TH2D("cResponseDiff_"+args.jetType, "cResponseDiff_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )


  numAbsEtaBins = len(etaList)-1

  for iEta, etaString in enumerate(etaList):
    lowEtaEdge = float(etaString.split('_')[0])/10.
    etaSpot = etaEdgeList.index(lowEtaEdge)


    gluonResponsePythia = respHists[0]["gluon"][etaString]
    lqResponsePythia = respHists[0]["LQ"][etaString]
    bResponsePythia = respHists[0]["b"][etaString]
    gluonResponseHerwig = respHists[1]["gluon"][etaString]
    lqResponseHerwig = respHists[1]["LQ"][etaString]
    bResponseHerwig = respHists[1]["b"][etaString]
    
    cResponsePythia = respHists[0]["c"][etaString]
    cResponseHerwig = respHists[1]["c"][etaString]

    #for iBin in [1]:
    #  gluonResponsePythia.SetBinContent(iBin, gluonResponsePythia.GetBinContent(2))
    #  gluonResponsePythia.SetBinError(iBin, gluonResponsePythia.GetBinError(2))
    #  lqResponsePythia.SetBinContent(iBin, lqResponsePythia.GetBinContent(2))
    #  lqResponsePythia.SetBinError(iBin, lqResponsePythia.GetBinError(2))
    #  gluonResponseHerwig.SetBinContent(iBin, gluonResponseHerwig.GetBinContent(2))
    #  gluonResponseHerwig.SetBinError(iBin, gluonResponseHerwig.GetBinError(2))

    gluonResponseDiff = ROOT.TH1D( "gluonResponseDiff_tmp", "gluonResponseDiff_tmp", len(ptEdgeList)-1, array.array('f', ptEdgeList) )
    lqResponseDiff = ROOT.TH1D( "lqResponseDiff_tmp", "lqResponseDiff_tmp", len(ptEdgeList)-1, array.array('f', ptEdgeList) )
    bResponseDiff = ROOT.TH1D( "bResponseDiff_tmp", "bResponseDiff_tmp", len(ptEdgeList)-1, array.array('f', ptEdgeList) )
    cResponseDiff = ROOT.TH1D( "cResponseDiff_tmp", "cResponseDiff_tmp", len(ptEdgeList)-1, array.array('f', ptEdgeList) )

    ## Derive the response difference histograms between Pythia and Herwig
    for iBin in range(1, len(ptEdgeList) ):
      if( gluonResponsePythia.GetBinContent(iBin) > 0 and gluonResponseHerwig.GetBinContent(iBin) > 0 ):
        gluonResponseDiff.SetBinContent(iBin, gluonResponsePythia.GetBinContent(iBin) - gluonResponseHerwig.GetBinContent(iBin) )
        gluonResponseDiff.SetBinError(iBin, gluonResponsePythia.GetBinError(iBin)**2 + gluonResponseHerwig.GetBinError(iBin)**2 )
      
      if( lqResponsePythia.GetBinContent(iBin) > 0 and lqResponseHerwig.GetBinContent(iBin) > 0 ):
        lqResponseDiff.SetBinContent(iBin, lqResponsePythia.GetBinContent(iBin) - lqResponseHerwig.GetBinContent(iBin) )
        lqResponseDiff.SetBinError(iBin, lqResponsePythia.GetBinError(iBin)**2 + lqResponseHerwig.GetBinError(iBin)**2 )

      if( bResponsePythia.GetBinContent(iBin) > 0 and bResponseHerwig.GetBinContent(iBin) > 0 ):
        bResponseDiff.SetBinContent(iBin, bResponsePythia.GetBinContent(iBin) - bResponseHerwig.GetBinContent(iBin) )
        bResponseDiff.SetBinError(iBin, bResponsePythia.GetBinError(iBin)**2 + bResponseHerwig.GetBinError(iBin)**2 )
      
      if( cResponsePythia.GetBinContent(iBin) > 0 and cResponseHerwig.GetBinContent(iBin) > 0 ):
        cResponseDiff.SetBinContent(iBin, cResponsePythia.GetBinContent(iBin) - cResponseHerwig.GetBinContent(iBin) )
        cResponseDiff.SetBinError(iBin, cResponsePythia.GetBinError(iBin)**2 + cResponseHerwig.GetBinError(iBin)**2 )

#    ## Freeze empty bins to be equal to last valid bin
#    for iBin in range(1, len(ptEdgeList)-1 ):
#      if gluonResponsePythia.GetBinContent(iBin+1) == 0.:
#        gluonResponsePythia.SetBinContent(iBin+1, gluonResponsePythia.GetBinContent(iBin) )
#        gluonResponsePythia.SetBinError(iBin+1, gluonResponsePythia.GetBinError(iBin) )
#
#      if lqResponsePythia.GetBinContent(iBin+1) == 0.:
#        lqResponsePythia.SetBinContent(iBin+1, lqResponsePythia.GetBinContent(iBin) )
#        lqResponsePythia.SetBinError(iBin+1, lqResponsePythia.GetBinError(iBin) )
#
#      if gluonResponseDiff.GetBinContent(iBin+1) == 0.:
#        gluonResponseDiff.SetBinContent(iBin+1, gluonResponseDiff.GetBinContent(iBin) )
#        gluonResponseDiff.SetBinError(iBin+1, gluonResponseDiff.GetBinError(iBin) )
#      
#      if lqResponseDiff.GetBinContent(iBin+1) == 0.:
#        lqResponseDiff.SetBinContent(iBin+1, lqResponseDiff.GetBinContent(iBin) )
#        lqResponseDiff.SetBinError(iBin+1, lqResponseDiff.GetBinError(iBin) )
#
#      if bResponseDiff.GetBinContent(iBin+1) == 0.:
#        bResponseDiff.SetBinContent(iBin+1, bResponseDiff.GetBinContent(iBin) )
#        bResponseDiff.SetBinError(iBin+1, bResponseDiff.GetBinError(iBin) )
#      
#      if cResponseDiff.GetBinContent(iBin+1) == 0.:
#        cResponseDiff.SetBinContent(iBin+1, cResponseDiff.GetBinContent(iBin) )
#        cResponseDiff.SetBinError(iBin+1, cResponseDiff.GetBinError(iBin) )

    ## Smooth in pt the 1D histograms
    gluonResponsePythia.Smooth(4)
    lqResponsePythia.Smooth(4)
    gluonResponseDiff.Smooth(4)
    lqResponseDiff.Smooth(4)
    bResponseDiff.Smooth(4)
    cResponseDiff.Smooth(4)

    ## Stitch together the histograms in eta
    for iBinX in range(1, len(ptEdgeList) ):
      gluonResponsePythia2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, gluonResponsePythia.GetBinContent(iBinX) )
      gluonResponsePythia2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, gluonResponsePythia.GetBinContent(iBinX) )

      lqResponsePythia2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, lqResponsePythia.GetBinContent(iBinX) )
      lqResponsePythia2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, lqResponsePythia.GetBinContent(iBinX) )

      gluonResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, gluonResponseDiff.GetBinContent(iBinX) )
      gluonResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, gluonResponseDiff.GetBinContent(iBinX) )

      lqResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, lqResponseDiff.GetBinContent(iBinX) )
      lqResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, lqResponseDiff.GetBinContent(iBinX) )

      bResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, bResponseDiff.GetBinContent(iBinX) )
      bResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, bResponseDiff.GetBinContent(iBinX) )

      cResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, cResponseDiff.GetBinContent(iBinX) )
      cResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, cResponseDiff.GetBinContent(iBinX) )

    gluonResponseDiff.Delete()
    lqResponseDiff.Delete()
    bResponseDiff.Delete()
    cResponseDiff.Delete()

  #gluonResponsePythia2D.Smooth()
  #lqResponsePythia2D.Smooth()
  #gluonResponseDiff2D.Smooth()
  #lqResponseDiff2D.Smooth()

  #Draw results
  c1 = ROOT.TCanvas()
  c1.SetLogx()
  c1.SetRightMargin(0.15)
  c1.SetLeftMargin(0.05)
#  gluonResponsePythia2D.GetZaxis().SetRangeUser(0.94,1.06)
  gluonResponsePythia2D.Draw("colz")
  c1.SaveAs(args.outDir+'/gluonResponsePythia.png')
  c1.SaveAs(args.outDir+'/gluonResponsePythia.eps')
#  lqResponsePythia2D.GetZaxis().SetRangeUser(0.97, 1.14)
  lqResponsePythia2D.Draw("colz")
  c1.SaveAs(args.outDir+'/lqResponsePythia.png')
  c1.SaveAs(args.outDir+'/lqResponsePythia.eps')
#  gluonResponseDiff2D.GetZaxis().SetRangeUser( -0.06, 0.01)
  gluonResponseDiff2D.Draw("colz")
  c1.SaveAs(args.outDir+'/gluonResponseDiff.png')
  c1.SaveAs(args.outDir+'/gluonResponseDiff.eps')
#  lqResponseDiff2D.GetZaxis().SetRangeUser( -0.015, 0.015)
  lqResponseDiff2D.Draw("colz")
  c1.SaveAs(args.outDir+'/lqResponseDiff.png')
  c1.SaveAs(args.outDir+'/lqResponseDiff.eps')
#  bResponseDiff2D.GetZaxis().SetRangeUser(-0.005, 0.035)
  bResponseDiff2D.Draw("colz")
  c1.SaveAs(args.outDir+'/bResponseDiff.png')
  c1.SaveAs(args.outDir+'/bResponseDiff.eps')
#  cResponseDiff2D.GetZaxis().SetRangeUser(-0.005, 0.035)
  cResponseDiff2D.Draw("colz")
  c1.SaveAs(args.outDir+'/cResponseDiff.png')
  c1.SaveAs(args.outDir+'/cResponseDiff.eps')


  ## Save results to file
  outFile = ROOT.TFile.Open(args.outDir+'/flavorUncertainties.root', "RECREATE")
  gluonResponsePythia2D.Write()
  lqResponsePythia2D.Write()
  gluonResponseDiff2D.Write()
  lqResponseDiff2D.Write()
  bResponseDiff2D.Write()
  cResponseDiff2D.Write()
  outFile.Close()

#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--path", dest='path', required=True, default="", help="Path of input files")
  parser.add_argument("--jetType", dest='jetType', required=True, default="AntiKt4EMTopo", help="Type of jet inputs, determining histogram names.  Like AntiKt4EMTopo.")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = args.path+'/GSCFlavorResults/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  deriveFlavorUncertainty()
  print("Done deriveFlavorUncertainty")
