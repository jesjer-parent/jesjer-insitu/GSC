import ROOT
import AtlasStyle
import argparse
import os

def plotSmoothedCurves():

  inputFile = ROOT.TFile.Open(args.input, "READ")
  inputHist = inputFile.Get("AntiKt4EMTopo_"+args.corrVar)


  etaEdges = inputHist.GetXaxis().GetXbins()
  ptEdges = inputHist.GetYaxis().GetXbins()
  varEdges = inputHist.GetZaxis().GetXbins()

  etaLabel = inputHist.GetXaxis().GetTitle()
  ptLabel  = inputHist.GetYaxis().GetTitle()
  varLabel = inputHist.GetZaxis().GetTitle()


  ###### Draw pt hists ###############
  ptToDraw = args.pts

  if ptToDraw == "all":
    ptToDraw = [ str(int(pt)+1) for pt in ptEdges]
    ptToDraw = ptToDraw[:-1]
    ptToDraw = ','.join(ptToDraw)
    print ptToDraw

  ptToDraw = ptToDraw.split(',')
  ptToDraw = [int(pt) for pt in ptToDraw]

  ROOT.gStyle.SetPalette(ROOT.kThermometer)
  ptColors = []
  for iPt, pt in enumerate(ptToDraw):
    ptColors.append( ROOT.gStyle.GetColorPalette( int( iPt*254/(len(ptToDraw)-1) ) ) )


  for etaBin in range(1, inputHist.GetNbinsX()+1 ):

    etaString = "|#eta| = {:.2f}".format( inputHist.GetXaxis().GetBinCenter(etaBin) )
    print inputHist.GetXaxis().GetBinCenter(etaBin)

    c1 = ROOT.TCanvas()
    c1.SetTopMargin(0.02)
    c1.SetRightMargin(0.03)
    c1.SetLeftMargin(0.13)
    c1.SetBottomMargin(0.15)

    leg = ROOT.TLegend(0.55, 0.55, 0.97, 0.95)
    leg.SetHeader("Jet p_{T} [GeV]")
    leg.SetFillStyle(0)
    leg.SetNColumns(4)
    for iPt, pt in enumerate(ptToDraw):
      ptBin = inputHist.GetYaxis().FindBin( pt )
      #if pt == inputHist.GetYaxis().GetBinUpEdge( ptBin ):
      #  ptBin -= 1

      hist = inputHist.ProjectionZ( "ptHist"+str(iPt), etaBin, etaBin, ptBin, ptBin)
      hist.SetMaximum(1.4)
      hist.SetMinimum(0.8)
      hist.SetLineColor( ptColors[iPt] )

      if( iPt%4 == 0):
        hist.SetLineStyle(1)
      elif( iPt%4 == 1):
        hist.SetLineStyle(7)
      elif( iPt%4 == 2):
        hist.SetLineStyle(2)
      elif( iPt%4 == 3):
        hist.SetLineStyle(3)

      hist.GetXaxis().SetTitle( varLabel )
      hist.GetYaxis().SetTitle( "Jet p_{T} response" )
      hist.GetYaxis().SetTitleOffset(0.9)

      if iPt == 0:
        hist.DrawCopy("histC")
      else:
        hist.DrawCopy("histCsame")

      ptString = str(int(round( inputHist.GetYaxis().GetBinCenter(ptBin))))
      leg.AddEntry( hist, ptString, "l")
      #if m_ptToDraw == "all":
      #  if (iPt%(len(ptToDraw)/nLeg)) == 0 or iPt == len(ptToDraw)-1:
      #    leg.AddEntry( hist, ptString, "l")
      #else:
      #  leg.AddEntry( hist, ptString, "l")

    leg.Draw()
    AtlasStyle.ATLAS_LABEL(0.15,0.92, 1, "Simulation Internal")
    AtlasStyle.myText(0.15, 0.87, 1, "Pythia Dijet #sqrt{s} = 13 TeV")
    AtlasStyle.myText(0.15, 0.82, 1, "EM+JES w/o GSC")
    AtlasStyle.myText(0.15, 0.77, 1, "#it{R}=0.4 anti-#it{k}_{T}, "+etaString)
    c1.SaveAs(args.outDir+"/GSCCalibration_"+args.corrVar+"_eta_"+str(etaBin-1)+'.png')


  ###### Draw eta hists ###############
  etaToDraw = args.etas

  if etaToDraw == "all":
    etaToDraw = [ "{:.2f}".format(eta) for eta in etaEdges]
    etaToDraw = etaToDraw[:-1]
    etaToDraw = ','.join(etaToDraw)
    print etaToDraw

  etaToDraw = etaToDraw.split(',')
  etaToDraw = [float(eta) for eta in etaToDraw]

  etaColors = []
  for iEta, eta in enumerate(etaToDraw):
    etaColors.append( ROOT.gStyle.GetColorPalette( int( iEta*254/(len(etaToDraw)-1) ) ) )


  for ptBin in range(1, inputHist.GetNbinsY()+1 ):

    ptString = "pt = "+str(int(inputHist.GetYaxis().GetBinCenter(ptBin) ))

    c1 = ROOT.TCanvas()
    c1.SetTopMargin(0.02)
    c1.SetRightMargin(0.03)
    c1.SetLeftMargin(0.13)
    c1.SetBottomMargin(0.15)

    leg = ROOT.TLegend(0.55, 0.55, 0.97, 0.95)
    leg.SetHeader("|#eta|")
    leg.SetFillStyle(0)
    leg.SetNColumns(3)
    for iEta, eta in enumerate(etaToDraw):
      etaBin = inputHist.GetXaxis().FindBin( eta )

      hist = inputHist.ProjectionZ( "etaHist"+str(iEta), etaBin, etaBin, ptBin, ptBin)
      hist.SetMaximum(1.4)
      hist.SetMinimum(0.8)
      hist.SetLineColor( etaColors[iEta] )

      if( iEta%3 == 0):
        hist.SetLineStyle(1)
      elif( iEta%3 == 1):
        hist.SetLineStyle(7)
      elif( iEta%3 == 2):
        hist.SetLineStyle(3)

      hist.GetXaxis().SetTitle( varLabel )
      hist.GetYaxis().SetTitle( "Jet p_{T} response" )
      hist.GetYaxis().SetTitleOffset(0.9)

      if iEta == 0:
        hist.DrawCopy("histC")
      else:
        hist.DrawCopy("histCsame")

      etaString = "{:.2f}".format(inputHist.GetXaxis().GetBinCenter(etaBin))
      leg.AddEntry( hist, etaString, "l")

    leg.Draw()
    AtlasStyle.ATLAS_LABEL(0.15,0.92, 1, "Simulation Internal")
    AtlasStyle.myText(0.15, 0.87, 1, "Pythia Dijet #sqrt{s} = 13 TeV")
    AtlasStyle.myText(0.15, 0.82, 1, "EM+JES w/o GSC")
    AtlasStyle.myText(0.15, 0.77, 1, "#it{R}=0.4 anti-#it{k}_{T}, "+ptString)
    c1.SaveAs(args.outDir+"/GSCCalibration_"+args.corrVar+"_pt_"+str(ptBin-1)+'.png')


#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--debug", dest='debug', action='store_true', default=False, help="More verbose for debugging.")
  parser.add_argument("--input", dest='input', required=True, default="", help="Input file name, should begin with Fitted_")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--correction", dest='corrVar', default="", required=True, help="Correction variable for the x-axis.  Example are Tile0 and nTrk")
  parser.add_argument("--pts", dest='pts', default="all", help="Pts to draw, either a comma separated list of values or all.")
  parser.add_argument("--etas", dest='etas', default="all", help="Etas to draw, either a comma separated list of values or all.")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = os.path.dirname(args.input)+'/plots/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning

  plotSmoothedCurves()
  print "Done plot smoothed GSC calibration curves"
