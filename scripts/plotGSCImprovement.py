import argparse
import ROOT
import AtlasStyle
import os
import math

def plotGSCImprovement():

  doRatio = True

  jetTypeString = args.jetType+"+JES"
  if( args.jetType == "PF"):
    corrTypes = ["MCJES", "CF", "Tile0", "EM3", "Ntrk", "Wtrk", "Nseg"]
    fileColors = [ROOT.kBlack, ROOT.kMagenta-5, ROOT.kRed-3, ROOT.kGreen-5, ROOT.kAzure-7, ROOT.kCyan-3, ROOT.kGray+1]
    inputFiles = ["CF/Fitted_EM3_CF.root",
                  "Tile0/Fitted_CF_Tile0.root",
                  "EM3/Fitted_Tile0_EM3.root",
                  "Ntrk/Fitted_EM3_Ntrk.root",
                  "Wtrk/Fitted_Ntrk_Wtrk.root",
                  "Nseg/Fitted_Wtrk_Nseg.root",
                  "Final/Fitted_EM3_Final.root"
                  ]
  elif( args.jetType == "EM"):
    corrTypes = ["MCJES", "Tile0", "EM3", "Ntrk", "Wtrk", "Nseg"]
    fileColors = [ROOT.kBlack, ROOT.kRed-3, ROOT.kGreen-5, ROOT.kAzure-7, ROOT.kCyan-3, ROOT.kGray+1, ROOT.kMagenta-5]
    inputFiles = ["Tile0/Fitted_EM3_Tile0.root",
                  "EM3/Fitted_Tile0_EM3.root",
                  "Ntrk/Fitted_EM3_Ntrk.root",
                  "Wtrk/Fitted_Ntrk_Wtrk.root",
                  "Nseg/Fitted_Wtrk_Nseg.root",
                  "Final/Fitted_EM3_Final.root"
                  ]
  elif( args.jetType == "LC"):
    corrTypes = ["MCJES", "Ntrk", "Wtrk", "Nseg"]
    fileColors = [ROOT.kBlack, ROOT.kAzure-7, ROOT.kCyan-3, ROOT.kGray+1, ROOT.kMagenta-5]
    inputFiles = ["Ntrk/Fitted_EM3_Ntrk.root",
                  "Wtrk/Fitted_Ntrk_Wtrk.root",
                  "Nseg/Fitted_Wtrk_Nseg.root",
                  "Final/Fitted_EM3_Final.root"
                  ]
  ## User specific inputs ##
  elif( args.jetType == "User"):
    args.jetType = ""
    jetTypeString = "MCJES"
    inputFiles = [
                   #"EM/Final/Fitted_EM3_Final.root",
                   #"LC/Final/Fitted_EM3_Final.root",
                   #"PF/Final/Fitted_EM3_Final.root"
                   "Fitted_Inclusive_FullSim_AntiKt4EMPFlow.root",
                   "Fitted_Inclusive_AFII_AntiKt4EMPFlow.root"
                   ]
    corrTypes = ["EM FullSim", "EM AFII"]
    fileColors = [ROOT.kGreen-3, ROOT.kRed]
    #corrTypes = ["EM #mu~14", "EM #mu~31", "PF #mu~14", "PF #mu~31", "LC #mu~14", "LC #mu~31"]
    #fileColors = [ROOT.kBlack, ROOT.kBlack, ROOT.kRed-3, ROOT.kRed-3, ROOT.kGreen-5, ROOT.kGreen-5 ]
    markerStyle = [20, 24, 21, 25, 22, 26]
    #corrTypes = ["EM #mu~14", "EM #mu~22", "EM #mu~31", "PF #mu~14", "PF #mu~22", "PF #mu~31"]
    #fileColors = [ROOT.kBlack, ROOT.kAzure-7, ROOT.kCyan-3, ROOT.kRed-3, ROOT.kGreen-5, ROOT.kOrange-3]

  else:
    print("Could not determine --jetType, please use an allowable arguement (EM, LC, PF, or User).")
    exit(1)

  inputFiles = [args.path +'/' + inputFile for inputFile in inputFiles]

  flavorTypes = ["All","LQ","gluon","b","c"]
  flavorString = ["all partons","light-quarks","gluons","b-quarks","c-quarks"]

  #3D file : responseType : eta
  respHists = []
  resoHists = []
  etaList = set([])

  ## Gather all input histograms from the various files ##
  for iF, inputFile in enumerate(inputFiles):
    inFile = ROOT.TFile.Open(inputFile, "READ")
    print(inputFile)
    keys = inFile.GetListOfKeys()
    respHists.append( {} )
    resoHists.append( {} )

    for flavType in flavorTypes:

      resoHists[iF][flavType] = {}
      respHists[iF][flavType] = {}

      respHistNames = [key.GetName() for key in keys if flavType+"_fitted_response" in key.GetName() ]
      resoHistNames = [key.GetName() for key in keys if flavType+"_fitted_resolution" in key.GetName()]

      for respHistName in respHistNames:
        etaString = '_'.join(respHistName.split('_')[-2:])
        etaList.add( etaString )

        hist = inFile.Get(respHistName)
        hist.SetDirectory(0)
        respHists[iF][flavType][etaString] = hist

      for resoHistName in resoHistNames:
        etaString = '_'.join(resoHistName.split('_')[-2:])
        etaList.add( etaString )

        hist = inFile.Get(resoHistName)
        hist.SetDirectory(0)
        resoHists[iF][flavType][etaString] = hist

  oneLine = ROOT.TF1("zl1","1", -10000, 100000)
  oneLine.SetTitle("")
  oneLine.SetLineWidth(1)
  oneLine.SetLineStyle(2)
  oneLine.SetLineColor(ROOT.kBlack)
  zeroLine = ROOT.TF1("zl0","0", -10000, 100000)
  zeroLine.SetTitle("")
  zeroLine.SetLineWidth(1)
  zeroLine.SetLineStyle(2)
  zeroLine.SetLineColor(ROOT.kBlack)

  etaList = list(etaList)
  ## Draw comparison hists between files for each flavType and eta ##
  #ROOT.gStyle.SetPalette(ROOT.kThermometer)
  #for iC, corrType in enumerate(corrTypes):
  #  fileColors.append( ROOT.gStyle.GetColorPalette( int( iC*254/(len(corrType)-1) ) ) )

  ####### Do inclusive / flavor response comparison ########
  print("Processing response plots")
  for etaString in etaList:

    for iF, flavType in enumerate(flavorTypes):
      c1 = ROOT.TCanvas()
      c1.SetTopMargin(0.02)
      c1.SetRightMargin(0.03)
      c1.SetLeftMargin(0.13)
      c1.SetBottomMargin(0.15)
      c1.SetLogx()

      ## Draw response plots ##
      leg = ROOT.TLegend(0.7, 0.65, 0.95, 0.95)
      leg.SetFillStyle(0)

      tmpHists = []
      for iC, corrType in enumerate(corrTypes):
        if not etaString in respHists[iC][flavType]:
          continue
        tmpHist = respHists[iC][flavType][etaString].Clone("tmp"+respHists[iC][flavType][etaString].GetName())
        tmpHists.append( tmpHist )

        tmpHist.GetXaxis().SetTitleOffset(1.)
        tmpHist.GetXaxis().SetTitle("p_{T}^{truth}")
        tmpHist.GetYaxis().SetTitleOffset(1.2)
        tmpHist.SetMarkerColor(fileColors[iC])
        tmpHist.SetLineColor(fileColors[iC])
        tmpHist.SetMarkerSize(1.)
        tmpHist.SetMarkerStyle(markerStyle[iC])
        tmpHist.GetYaxis().SetRangeUser(0.9,1.2)
#        tmpHist.GetXaxis().SetRangeUser(25,7000)
        if iC == 0:
          tmpHist.DrawCopy("p")
        else:
          tmpHist.DrawCopy("psame")

        leg.AddEntry(tmpHist, corrType, "p")

      ## Get correct eta values ##
      etaLow = int(etaString.split('_')[0])/10.
      etaHigh = int(etaString.split('_')[1])/10.

      leg.Draw()
      oneLine.Draw("same")

      etaLow = int(etaString.split('_')[0])/10.
      etaHigh = int(etaString.split('_')[1])/10.
      AtlasStyle.ATLAS_LABEL(0.35,0.92, 1.1, "Simulation Internal")
      AtlasStyle.myText(0.35, 0.85, 1., "Pythia Dijet #sqrt{s} = 13 TeV")
      AtlasStyle.myText(0.35, 0.79, 1., "#it{R}=0.4 anti-#it{k}_{t} "+jetTypeString)
      AtlasStyle.myText(0.16, 0.92, 1.3, flavorString[iF])
      AtlasStyle.myText(0.16, 0.86, 1.1, str(etaLow)+' < |#eta| < '+str(etaHigh))

      c1.SaveAs(args.outDir+"/response_"+flavType+'_'+etaString+'.png')
#      c1.SaveAs(args.outDir+"/response_"+flavType+'_'+etaString+'.eps')
      c1.Clear()
      leg.Clear()

  ####### Do resolution comparison ########
  print("Processing resolution plots")
  for etaString in etaList:
    for iF, flavType in enumerate(flavorTypes):
      c1 = ROOT.TCanvas()
      c1.SetTopMargin(0.02)
      c1.SetRightMargin(0.03)
      c1.SetLeftMargin(0.13)
      c1.SetBottomMargin(0.15)

      c1.SetLogx()

      ## Draw response plots ##
      leg = ROOT.TLegend(0.7, 0.65, 0.95, 0.95)
      leg.SetFillStyle(0)

      ratioleg = ROOT.TLegend(0.7, 0.6, 0.95, 1.)
      ratioleg.SetFillStyle(0)

      if(doRatio):
        pad1 = ROOT.TPad("pad1","pad1",0,0.36,1,0.995)
        pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.35)
        pad1.SetLogx()
        pad2.SetLogx()
        pad1.Draw()
        pad2.Draw()
        pad1.SetBottomMargin(0.02)
        pad1.SetTopMargin(0.02)
        pad2.SetTopMargin(0.03)
        pad1.SetRightMargin(0.03)
        pad2.SetRightMargin(0.03)
        pad1.SetLeftMargin(0.13)
        pad2.SetLeftMargin(0.13)
        pad2.SetBottomMargin(.45)
        zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
        zeroLine.SetTitle("")
        zeroLine.SetLineWidth(1)
        zeroLine.SetLineStyle(7)
        zeroLine.SetLineColor(ROOT.kBlack)

      isFirst = True
      ratioHist = []
      for iC, corrType in enumerate(corrTypes):

        if(doRatio):
          pad1.cd()

        if not etaString in resoHists[iC][flavType]:
          continue
        resoHists[iC][flavType][etaString].GetXaxis().SetTitleOffset(1.)
        resoHists[iC][flavType][etaString].GetXaxis().SetTitle("p_{T}^{truth}")
        resoHists[iC][flavType][etaString].GetYaxis().SetTitleOffset(1.2)
        resoHists[iC][flavType][etaString].SetMarkerColor(fileColors[iC])
        resoHists[iC][flavType][etaString].SetLineColor(fileColors[iC])
        resoHists[iC][flavType][etaString].SetMarkerSize(0.7)
        resoHists[iC][flavType][etaString].SetMarkerSize(1.)
        resoHists[iC][flavType][etaString].SetMarkerStyle(markerStyle[iC])
        resoHists[iC][flavType][etaString].GetYaxis().SetRangeUser(0.0,0.4)
        resoHists[iC][flavType][etaString].GetXaxis().SetLabelSize(0)
        #resoHists[iC][flavType][etaString].GetXaxis().SetRangeUser(25,7000)
        if isFirst:
          isFirst = False
          resoHists[iC][flavType][etaString].Draw("p")
        else:
          resoHists[iC][flavType][etaString].Draw("psame")

        leg.AddEntry(resoHists[iC][flavType][etaString], corrType, "p")

        if(doRatio):
          pad2.cd()
          if not isFirst:
            newHist = resoHists[iC][flavType][etaString].Clone(resoHists[iC][flavType][etaString].GetName()+'_ratio')
            ratioHist.append(newHist)

            newHist.GetYaxis().SetNdivisions(4)
            newHist.GetYaxis().SetLabelSize(0.1)
            newHist.GetXaxis().SetLabelSize(0.12)
            newHist.GetXaxis().SetTitleSize(0.12)
            newHist.GetYaxis().SetTitleSize(0.1)
            newHist.GetYaxis().SetTitleOffset(0.6)
            newHist.SetMarkerSize(0.7)
            newHist.SetMarkerStyle(20)
            newHist.GetYaxis().SetTitle( "#splitline{sgn(#sigma'-#sigma)#sqrt{#sigma'^{2}-#sigma^{2}}}{MC16d vs MC16a}")

            iR = 0
            if iC == 2 or iC == 4:
              continue
            elif iC == 3:
              iR = 2
            elif iC == 5:
              iR = 4

            for i in range(1, newHist.GetNbinsX()+1 ):
              value = newHist.GetBinContent(i)**2
              nom = resoHists[iR][flavType][etaString].GetBinContent(i)**2
              newValue = math.sqrt(math.fabs(value-nom))
              newError = math.sqrt( newHist.GetBinError(i)**2 + resoHists[iR][flavType][etaString].GetBinError(i)**2)
              if value < nom:
                newValue *= -1
              newHist.SetBinContent(i, newValue)
              newHist.SetBinError(i, newError)

            newHist.GetYaxis().SetRangeUser(-0.05, 0.15)
            ##JD newHist.GetYaxis().SetRangeUser(-0.25, 0.1)
#            newHist.GetXaxis().SetRangeUser(25,7000)
#            newHist.GetYaxis().SetRangeUser(-0.05, -0.02)
            if iC == 1:
              newHist.Draw("p")
            else:
              newHist.Draw("psame")

            #if iC == 1:
            #  ratioleg.AddEntry(newHist, "LC", "p")
            #elif iC == 3:
            #  ratioleg.AddEntry(newHist, "EM", "p")
            #elif iC == 5:
            #  ratioleg.AddEntry(newHist, "PF", "p")
            zeroLine.Draw("same")




      c1.cd()
      if(doRatio):
        pad1.Update()
        pad2.Update()
        pad1.cd()
      leg.Draw()

      etaLow = int(etaString.split('_')[0])/10.
      etaHigh = int(etaString.split('_')[1])/10.
      if etaLow == 0.1:
        etaLow = 0.
      AtlasStyle.ATLAS_LABEL(0.35,0.88, 1.7, "Simulation Internal")
      AtlasStyle.myText(0.35, 0.81, 1.5, "Pythia Dijet #sqrt{s} = 13 TeV")
      AtlasStyle.myText(0.35, 0.74, 1.5, "#it{R}=0.4 anti-#it{k}_{t} "+jetTypeString)
      AtlasStyle.myText(0.16, 0.88, 1.8, flavorString[iF])
      AtlasStyle.myText(0.16, 0.81, 1.6, str(etaLow)+' < |#eta| < '+str(etaHigh))


      if(doRatio):
        pad2.cd()
        ratioleg.Draw()


      c1.SaveAs(args.outDir+"/resolution_"+flavType+'_'+etaString+'.png')
#      c1.SaveAs(args.outDir+"/resolution_"+flavType+'_'+etaString+'.eps')

      if(doRatio):
        pad1.Clear()
        pad2.Clear()
      c1.Clear()
      c1.cd()





#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--path", dest='path', required=True, default="", help="Path of input files")
  parser.add_argument("--jetType", dest='jetType', required=True, default="", help="Type of jet inputs, determining input file names.  Can be EM, LC, PF, or User (for user specific fitting")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = args.path+'/GSCImprovementPlots/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  plotGSCImprovement()
  print("Done plot GSC Improvement")
