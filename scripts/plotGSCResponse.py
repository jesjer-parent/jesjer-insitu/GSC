import argparse
import ROOT
import AtlasStyle
import os

def plotGSCResponse():

  inputFile = ROOT.TFile.Open(args.input, "READ")
  if args.distribution:
    histTag = "h_response_vs_"+args.corrVar #!!
  elif args.corrVar == "Nseg":
    histTag = "g_resp_vs_"+args.corrVar
  else:
    histTag = "g_respCorrected_vs_"+args.corrVar


  ###### Draw pt hists ###############
  ptToDraw = args.pts.split(',')

  ptColors = [ROOT.kBlue, ROOT.kRed, ROOT.kBlack, ROOT.kGreen+2]

  ## Reverse Ntrk to fix x-axis ##
  if args.corrVar == "Ntrk":
    ptColors = list(reversed(ptColors))
    ptToDraw = list(reversed(ptToDraw))

  if len(ptToDraw) >= 5:
    ROOT.gStyle.SetPalette(ROOT.kThermometer)
    for iPt, pt in enumerate(ptToDraw):
      ptColors.append( ROOT.gStyle.GetColorPalette( int( iPt*254/(len(ptToDraw)-1) ) ) )

  ## Get eta values ##
  hists = []
  etaRanges = []
  for ptStr in ptToDraw:
    hists.append( [] )
    for key in inputFile.GetListOfKeys():
      histName = key.GetName()
      if histTag in histName and ptStr in histName and (args.distribution == False or "_proj_var" in histName): #!!
        hist = inputFile.Get( histName )
        hists[-1].append( hist )
        etaRange = histName.split('_eta_')[1]
        etaRange = '_'.join(etaRange.split('_')[0:2])
        etaRanges.append( etaRange )

  etaRanges = list(set(etaRanges))
  etaRanges = [etaRange for (etaLow,etaRange) in sorted(zip([int(etaRange.split('_')[0]) for etaRange in etaRanges ] ,etaRanges))]

  for iEta, etaRange in enumerate(etaRanges):
    etaValues = etaRange.split('_')
    etaValues = [float(eta)/10. for eta in etaValues]
    etaString = "{:.1f} < |#eta| < {:.1f}".format( etaValues[0], etaValues[1] )

    c1 = ROOT.TCanvas()
    c1.SetTopMargin(0.02)
    c1.SetRightMargin(0.03)
    c1.SetLeftMargin(0.13)
    c1.SetBottomMargin(0.15)

    leg = ROOT.TLegend(0.55, 0.7, 0.88, 0.95)
    leg.SetFillStyle(0)

#    thisMin, thisMax = 0, 0
#    for iPt, ptStr in enumerate(ptToDraw):
#      inputHist = [hist for hist in hists[iPt] if etaRange in hist.GetName()][0]
#      thisMin = min(thisMin, inputHist.GetXaxis().GetBinLowEdge(1))
#      thisMax = max(thisMax, inputHist.GetXaxis().GetBinUpEdge(inputHist.GetNbinsX()))
#
#    thisMin = thisMin - (thisMax-thisMin)/10.
#    thisMax = thisMax + (thisMax-thisMin)/10.

    for iPt, ptStr in enumerate(ptToDraw):
      inputHist = [hist for hist in hists[iPt] if etaRange in hist.GetName()][0]
      if args.distribution and inputHist.Integral() > 0:
        if args.corrVar == "Nseg":
          inputHist.Scale( 1./inputHist.Integral(inputHist.FindBin(10),inputHist.GetNbinsX()))
          inputHist.SetMaximum( inputHist.GetBinContent(inputHist.FindBin(10))*50)
          inputHist.SetMinimum(0.0001)
        else:
          inputHist.Scale( 1./inputHist.Integral())
          inputHist.SetMaximum( inputHist.GetMaximum()*3)

      if not args.distribution:
        inputHist.SetMaximum(1.4)
        inputHist.SetMinimum(0.8)
      if args.corrVar == "Nseg":
        c1.SetLogx()
        if args.distribution:
          c1.SetLogy()
        inputHist.GetXaxis().SetMoreLogLabels()
        inputHist.GetXaxis().SetRangeUser(10., 200.)
        if(not args.distribution):
          inputHist.GetYaxis().SetRangeUser(0.7, 1.4)

      inputHist.SetLineColor( ptColors[iPt] )
      inputHist.SetMarkerColor( ptColors[iPt] )
      inputHist.GetYaxis().SetTitleOffset(0.9)
      if args.distribution:
        inputHist.GetYaxis().SetTitle("Normalized entries")
      else:
        inputHist.GetYaxis().SetTitle("Jet p_{T} Response")
      if args.corrVar == "Nseg":
        inputHist.GetXaxis().SetTitle("N_{seg}")
      elif args.corrVar == "Ntrk":
        inputHist.GetXaxis().SetTitle("N_{trk}")
      elif args.corrVar == "Tile0":
        inputHist.GetXaxis().SetTitle("f_{Tile0}")
      elif args.corrVar == "EM3":
        inputHist.GetXaxis().SetTitle("f_{EM3}")
      elif args.corrVar == "Wtrk":
        inputHist.GetXaxis().SetTitle("W_{trk}")
      else:
        inputHist.GetXaxis().SetTitle(args.corrVar)

#      inputHist.GetXaxis().SetRangeUser(thisMin, thisMax)


      if args.distribution:
        inputHist.Draw("psame")
      elif iPt == 0:
        inputHist.Draw("ap")
      else:
        inputHist.Draw("p")

      if args.corrVar == "Nseg":
        leg.AddEntry( inputHist, ptStr.replace('_',' < E^{truth} < ')+' GeV', "pl")
      else:
        leg.AddEntry( inputHist, ptStr.replace('_',' < p_{T}^{truth} < ')+' GeV', "pl")

    leg.Draw()
    AtlasStyle.ATLAS_LABEL(0.17,0.92, 1, "Simulation Internal")
    AtlasStyle.myText(0.17, 0.87, 1, "Pythia Dijet #sqrt{s} = 13 TeV")
    AtlasStyle.myText(0.17, 0.82, 1, args.jetType+"+JES")
    AtlasStyle.myText(0.17, 0.77, 1, "#it{R}=0.4 anti-#it{k}_{t}, "+etaString)
    if (args.distribution):
      c1.SaveAs(args.outDir+"/Distribution_"+args.corrVar+"_eta_"+str(iEta)+'.png')
      c1.SaveAs(args.outDir+"/Distribution_"+args.corrVar+"_eta_"+str(iEta)+'.eps')
    else:
      c1.SaveAs(args.outDir+"/GSCResponse_"+args.corrVar+"_eta_"+str(iEta)+'.png') #!!
      c1.SaveAs(args.outDir+"/GSCResponse_"+args.corrVar+"_eta_"+str(iEta)+'.eps') #!!



#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--debug", dest='debug', action='store_true', default=False, help="More verbose for debugging.")
  parser.add_argument("--input", dest='input', required=True, default="", help="Input file name, should begin with Fitted_")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--correction", dest='corrVar', default="", required=True, help="Correction variable for the x-axis.  Example are Tile0 and nTrk")
  parser.add_argument("--pts", dest='pts', default="30_40,80_100,350_400,1000_1200", help="Pts to draw.")
  parser.add_argument("--jetType", dest='jetType', default="EM", help="Jet type for plot label")
  parser.add_argument("-d", dest='distribution', action='store_true', default=False, help="Plot variable distribution instead of response.")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = os.path.dirname(args.input)+'/plots/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  if args.corrVar == "Nseg":
    args.pts = "200_400,600_80,1000_1200,1600_2000"

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  plotGSCResponse()
  print "Done plot GSC Response"
