#!/usr/bin/env python

###################################################################
# scaleHist.py                                                   #
# A MJB second stage python script                                #
# Author Jeff Dandoy, UChicago                                    #
#                                                                 #
# This script takes the output of MultijetBalanceAlgo and scales  #
# the MC according to the number of events that were run on.      #
# Output files may then be combined directly with hadd            #
#                                                                 #
###################################################################

import os, glob, argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--path", dest='path', default="",
         help="Path to input files")
args = parser.parse_args()

from ROOT import *

#def scaleHist(files):

inFileNames = glob.glob(args.path+'/*.root')
for file in inFileNames:

  inFile = TFile.Open(file, "READ");
  outFile = TFile.Open(file[:-5]+".scaled.root", "RECREATE");

  keyList = [key.GetName() for key in inFile.GetListOfKeys()] #List of top level objects

  #Get cutflow histogram for MC scaling
  cutflowName = "h_number_of_events"
  thisCutflow = inFile.Get( cutflowName )
  if not thisCutflow:
    print "Error, could not find ", cutflowName, "in", file
    exit(1)
  numEvents = thisCutflow.GetBinContent(1)
  scaleFactor = 1./numEvents


  print "Scaling and saving hists for file ", file
  for key in keyList:
    thisHist = inFile.Get(key)
    if( thisHist.InheritsFrom("TTree") ):
      continue
#    if not "h_respBig_vs_pttruth" in key:
#      continue
    thisHist.SetDirectory( outFile )
    if (thisHist.InheritsFrom("TH1") or thisHist.InheritsFrom("TH2")) and (thisHist.GetEffectiveEntries() != thisHist.GetEntries() ): #weighted histograms
      thisHist.Scale( scaleFactor )

  outFile.Write()
  outFile.Close()
  inFile.Close()

