import argparse, os, math, array
import ROOT
import AtlasStyle
corrType = "respCorrected"

#---------------------------------------------------------------------------------
## Top level GSCSmoothing function.  Loads the TGraphs with the GraphHandler class,
## Smooths linear pt TGraphs with LinearSmoother, and smooths the 2D pt vs xVar
## TGraph with GaussSmoother
def RunGSCSmoothing():

  print "----------------------------------"
  print "   GSC Smoothing Tool"
  print "----------------------------------"

  if args.corrVar == "Nseg":
    NpTbins = 20
    Nbins = 40
  else:
    NpTbins = 30
    Nbins = 50

  #Nbins = 30
  #!NpTbins = 40

  kernel_fraction = 10
  #kernel_fraction = 6
  pTkernel = 0.07
  jetVarKernel = 0 # Uses a jet property kernel based on the min/max of the graph if left at 0

  ## Create tool to get input TGraphs ##
  GraphHandle = GraphHandler(args.input, args.ptMin)

  #---------------------------------------------------------------------------------
  ## For each eta bin, a linear smooth will be perfomed on each pt bin,
  ## and a gaussian smoothing will be applied to the 2D histogram ##
  print "Running GSCSmooth"

  ## Make output file ##
  outFileName = args.outDir+'/Smoothed_'+os.path.basename(args.input);
  outputFile = ROOT.TFile(outFileName, "RECREATE")

  ## Get Eta ranges from available TGraphs in input file ##
  etaRanges = GraphHandle.getEtaRanges()

  ## Create output pdf for displaying the result of the smoothingg ##
  can = ROOT.TCanvas()
  psName = args.outDir+"/Smoothed_plots_"+args.corrVar+".pdf"
  can.Print(psName+"[");

  smoothedHistList = []
  ## Loop over each eta bin ##
  for ieta, etaRange in enumerate(etaRanges):
    print "Running on eta range ", etaRange

    ## Load input graphs of response vs xVar, one for each pt range ##
    graphs = GraphHandle.loadGraphs(etaRange);
    xvar_min, xvar_max = GraphHandle.GetMinMax(graphs);

    ## Create linear smoother tool ##
    LinearSmoother = LinearSmoothing("Smoother_"+etaRange, xvar_min, xvar_max, kernel_fraction);

    #print "Linear Smoother on", len(graphs), "graphs"
    ## Get linearly smoothed histograms from TGraphs ##
    smoothedHists = []
    kernelWidths = []
    for graph in graphs:
      smoothedHists.append( LinearSmoother.MakeSmoothHisto(graph, 50) )
      kernelWidths.append( LinearSmoother.kernel )

    ## Build 2D graphs from smooth histograms ##
    resp2D = GraphHandle.BuildGraph2D(smoothedHists);

    ## set jetVarKernel based on range of xVar ##
    #jetVarKernel = (resp2D.GetYaxis().GetXmax() - resp2D.GetYaxis().GetXmin())/25
    jetVarKernel = (resp2D.GetYaxis().GetXmax() - resp2D.GetYaxis().GetXmin())/50

    ## Do Gaussian smoothing ##
    print "GaussianSmoothing"
    GaussSmoother = GaussianSmoothing(pTkernel, jetVarKernel, NpTbins, Nbins)

    histoName = args.jetType+'_'+args.corrVar+'_interpolation_resp_eta_'+etaRange

    pTmin = GraphHandle.pTlowbinedges[0]
    pTmax = GraphHandle.pThighbinedges[-1]
  #Why is this needed?
#!!    if ieta == 0:
#!!      pTmin = GraphHandle.pTlowbinedges[0]
#!!      pTmax = GraphHandle.pThighbinedges[-1]
#!!    else:
#!!      pTmin = smoothedHistList[0].GetXaxis().GetBinLowEdge(1)
#!!      pTmax = smoothedHistList[0].GetXaxis().GetBinUpEdge(smoothedHistList[0].GetNbinsX())


    print "Smoothing from ", pTmin, pTmax, GraphHandle.xLow_original, GraphHandle.xHigh_original
    smoothed2DHist = GaussSmoother.SmoothPtJetPropertyGraph(resp2D, histoName, pTmin, pTmax, GraphHandle.xLow_original, GraphHandle.xHigh_original)
    print "Done GaussianSmoothing"

    #---------------------------------------------------------------------------------
    ## Draw the results to output file and the TCanvas ##
    smoothedHistList.append( smoothed2DHist )
    ## Remove upper eta bin for Jona JetCalibTool input ##
    smoothed2DHist.SetName( '_'.join(smoothed2DHist.GetName().split('_')[:-1]) )
    smoothed2DHist.Write()

    ptbins = GraphHandle.pTbincenters
    for ipt, pt in enumerate(ptbins):
      thisBin = smoothed2DHist.GetXaxis().FindBin(pt)
      if (args.debug):
        print "Drawing bin", thisBin

      jetTypeStr = ""
      if args.jetType == "AntiKt4EMTopo":
        jetTypeStr = "EM"
      elif args.jetType == "AntiKt4LCTopo":
        jetTypeStr = "LC"
      elif args.jetType == "AntiKt4EMPFlow":
        jetTypeStr = "PF"
      else:
        print "Error, don't recognize jet type", args.jetType
        exit(1)

      ## Get a projection along a pt bin ##
      proj = smoothed2DHist.ProjectionY( "proj_eta_"+etaRange+'_'+str(ipt), thisBin, thisBin)
      proj.SetStats(0)
      proj.GetYaxis().SetRangeUser(0.70,1.3);
      proj.GetYaxis().SetTitle("Jet response at "+jetTypeStr+"+JES scale");
      proj.SetLineColor(ROOT.kBlue)
      proj.SetLineWidth(2)

      ## Get and configure the input TGraph ##
      graph = GraphHandle.graphs[ipt]
      graph.SetMarkerStyle(20)
      graph.SetMarkerSize(0.5);
      graph.SetMarkerColor(ROOT.kBlack)
      graph.SetLineColor(ROOT.kBlack)

      ## Get the linear smoothed Histgoram (no gaussian smoothing) ##
      smoothedHists[ipt].SetLineColor(ROOT.kRed)
      smoothedHists[ipt].GetYaxis().SetRangeUser(0.70,1.3);


      lowEdge = smoothedHists[ipt].GetXaxis().GetBinLowEdge(1)
      upEdge = smoothedHists[ipt].GetXaxis().GetBinUpEdge(smoothedHists[ipt].GetNbinsX()+1)
      diffEdge = (upEdge-lowEdge)/20.

      if( args.corrVar != "Nseg"):
        proj.GetXaxis().SetRangeUser(lowEdge-diffEdge, upEdge+diffEdge);

      proj.Draw("l")
      graph.Draw("pl")
      smoothedHists[ipt].Draw("same histl")

      #smoothedHists[ipt].Draw("histl")
      #proj.Draw("histsamel")
      #graph.Draw("pl")

      ## Draw information to the canvas ##
      chi2  = GraphHandle.Chi2(graph, proj)
      pTmin = GraphHandle.pTlowbinedges[ipt]
      pTmax = GraphHandle.pThighbinedges[ipt]

      etaLow = float(etaRange.split('_')[0])
      etaHigh = float(etaRange.split('_')[1])
      AtlasStyle.myText(0.5, 0.9,  0.8, "Anti-k_{T} R=0.4 "+jetTypeStr+"+JES" )
      AtlasStyle.myText(0.5, 0.85, 0.8, "%.1f < |#eta| < %.1f" %(0.1*etaLow,0.1*etaHigh) )
      if( args.corrVar == "Nseg"):
        AtlasStyle.myText(0.5, 0.8,  0.8, "%.0f GeV < E^{truth} < %.0f GeV" %(pTmin, pTmax) )
      else:
        AtlasStyle.myText(0.5, 0.8,  0.8, "%.0f GeV < p_{T}^{truth} < %.0f GeV" %(pTmin, pTmax) )
      AtlasStyle.myText(0.5, 0.75, 0.8, "Kernel width = %.3f, linear extrapolation" %(kernelWidths[ipt]) )
      AtlasStyle.myText(0.5, 0.7,  0.8, "pT kernel = %.3f, %s kernel = %.3f" %(pTkernel, args.corrVar, jetVarKernel) )
      if( graph.GetN() > 2 ):
        AtlasStyle.myText(0.5, 0.65, 0.8, "#it{#chi}^{2} / #it{n}_{dof} = %.3f    P_{#it{#chi}^{2}} = %.1f%%" %(chi2/(graph.GetN()-2), ROOT.TMath.Prob(chi2,graph.GetN()-2)*100 ) )
      else:
        AtlasStyle.myText(0.5, 0.65, 0.8, "#it{#chi}^{2} = %.3f" %(chi2) )

      ROOT.gPad.Print(psName);

  ## Save the TCanvas and close the output file ##
  can.Print(psName+"]");
  print "Produced pdf", psName

  ## Get binning for 3D histogram ##
  etaBins = []
  etaBins.append( float(etaRanges[0].split('_')[0])/10. )
  for etaRange in etaRanges:
    etaBins.append( float(etaRange.split('_')[1])/10. )
  etaBinsArray = array.array('d', etaBins )

  ptBins = []
  ptBins.append( smoothedHistList[0].GetXaxis().GetBinLowEdge(1) )
  for iBin in range(1, smoothedHistList[0].GetNbinsX()+1 ):
    ptBins.append( smoothedHistList[0].GetXaxis().GetBinUpEdge( iBin ) )
  ptBinsArray = array.array('d', ptBins )

  corrVarBins = []
  corrVarBins.append( smoothedHistList[0].GetYaxis().GetBinLowEdge(1) )
  for iBin in range(1, smoothedHistList[0].GetNbinsY()+1 ):
    corrVarBins.append( smoothedHistList[0].GetYaxis().GetBinUpEdge( iBin ) )
  corrVarBinsArray = array.array('d', corrVarBins )

  correctionHist = ROOT.TH3F( args.jetType+'_'+args.corrVar, args.jetType+'_'+args.corrVar, len(etaBins)-1, etaBinsArray, len(ptBins)-1, ptBinsArray, len(corrVarBins)-1, corrVarBinsArray)
  for iHist, hist in enumerate( smoothedHistList ):
    etaBin = iHist + 1
    for ptBin in range(1, hist.GetNbinsX()+1 ):
      for corrVarBin in range(1, hist.GetNbinsY()+1 ):
        correctionHist.SetBinContent(etaBin, ptBin, corrVarBin, hist.GetBinContent(ptBin, corrVarBin) )
        correctionHist.SetBinError(etaBin, ptBin, corrVarBin, hist.GetBinError(ptBin, corrVarBin) )

  correctionHist.GetXaxis().SetTitle( "|#eta_{det}|" )
  correctionHist.GetYaxis().SetTitle( smoothedHistList[0].GetXaxis().GetTitle() )
  correctionHist.GetZaxis().SetTitle( smoothedHistList[0].GetYaxis().GetTitle() )

  correctionHist.Write()

  outputFile.Close();
  print "Finished smoothing, produced root file", outFileName



#-----------------------------------------------------------------------------------------------------------
#################################### GraphHandler Class #####################################################
## The GraphHandler class collects the input TGraphs for each eta bin and parsing their correct pt ranges  ##
class GraphHandler():
  def __init__(self, infile, pTmin = 20):

    self.debug = args.debug
    self.truth = False
    self.pTmin = pTmin
    self.xHigh_original = None
    self.xLow_original = None

    self.inputFile = ROOT.TFile.Open(infile, "READ")
    if not self.inputFile:
      print "Error, could not open", infile
      exit(1)

    self.keyNames = [key.GetName() for key in self.inputFile.GetListOfKeys()]

    self.pTbincenters = []
    self.pTlowbinedges = []
    self.pThighbinedges = []

    self.graphs = []
    self.graphNames = []

  #---------------------------------------------------------------------------------
  ## This function returns the chi2 of all pionts between a graph and interpolated histogram ##
  def Chi2(self, g, smoothHist):
    chi2 = 0
    for i in range(0, g.GetN()):
      x = g.GetX()[i]
      R = g.GetY()[i]
      dR = g.GetEY()[i]
      Rsmooth = smoothHist.Interpolate(x);
      chi2 += ((Rsmooth-R)/dR)**2
    return chi2

  #---------------------------------------------------------------------------------
  ## This function builds a 2D TGraphError using several input TH1s ##
  def BuildGraph2D(self, smoothTH1s):

    resp2D = ROOT.TGraph2DErrors()

    if len(self.graphs) == 0:
      print "Error, no graphs have been loaded.  First load them for this eta value with GraphHandler.loadGraphs(eta)"
      exit(1)


    print "Bins are",
    ## For each pt region ##
    for iH, hist in enumerate(smoothTH1s):

      pT = self.pTbincenters[iH]
      print str(iH)+':'+str(pT),
      ## For each var region (i.e. Tile0) ##
      for iX in range(hist.GetNbinsX()):

        jetVar = hist.GetBinCenter(iX)
        resp = hist.GetBinContent(iX)
        error = hist.GetBinError(iX)


        if(resp > 0):
          n = resp2D.GetN()
          if(self.truth):
            resp2D.SetPoint(n, pT, jetVar, resp)
          else:
            resp2D.SetPoint(n, pT*resp, jetVar, resp)
            #resp2D.SetPoint(n, pT/resp, jetVar, resp) ##Numerical inversion on bin center?  !!Should be multiply, and should be bin center.  Just take this from the other graph?
          resp2D.SetPointError(n, 0, 0, error)

    print ""
    if( args.corrVar == "Nseg"):
      resp2D.GetXaxis().SetTitle( "Jet E" )
    else:
      resp2D.GetXaxis().SetTitle( "Jet p_{T}" )
    resp2D.GetYaxis().SetTitle( smoothTH1s[0].GetXaxis().GetTitle() )
    resp2D.GetZaxis().SetTitle( smoothTH1s[0].GetYaxis().GetTitle() )

    return resp2D

  #---------------------------------------------------------------------------------
  ## This function will taken a TGraphError (or a symmetric TGraphAsymError), and create a new TGraphError using only
  ## datapoints with a response above 0.
  def BuildCleanGraph(self, g):


    if type(g) == ROOT.TGraphAsymmErrors:
      if(self.debug):
        print "Reformatting TGraphAsymmErrors, ", g
      respErrorHigh = g.GetEYhigh()
      respErrorLow  = g.GetEYlow()
    elif type(g) == ROOT.TGraphErrors:
      if(self.debug):
        print "Reformatting TGraphErrors"
      respErrorHigh = g.GetEY()
      respErrorLow  = g.GetEY()

    newGraph = ROOT.TGraphErrors()

    g_x = g.GetX()
    g_y = g.GetY()
    g_ey = g.GetEY()

    for i in range(0, g.GetN() ):
      ## If there is an asymmetric error, exit.  Why?! ##
      if( respErrorHigh[i] != respErrorLow[i]):
        print "Incompatible error bars"
        exit(1)

      jetVar = g_x[i]
      response = g_y[i]

      ## These should be symmetric to get to this point, so why would we set it to zero?
      if type(g) == ROOT.TGraphAsymmErrors:
        #responseError = 0  # Orignal code
        responseError = respErrorHigh[i] # New code
      elif type(g) == ROOT.TGraphErrors:
        responseError = g_ey[i]

      if(response==0):
        if(self.debug):
          print "Response is zero for pt bin", g.GetName(), ". Skipping this point (this is OK)."
        continue
      elif(response<0):
        print "Error, found negative response in graph", g.GetName(), "please debug"
        exit(1)

      ng = newGraph.GetN()
      newGraph.SetPoint(ng, jetVar, response)
      newGraph.SetPointError(ng, 0, responseError)

    if(newGraph.GetN()<4 and args.corrVar != "Nseg"):
      return 0
    elif newGraph.GetN()<3:
      return 0

    newGraph.GetXaxis().SetTitle( g.GetXaxis().GetTitle() )
    newGraph.GetYaxis().SetTitle( g.GetYaxis().GetTitle() )

    return newGraph;

  def getEtaRanges(self):
    if( self.debug ):
      print "getEtaRanges"

    etaRanges = [ '_'.join(keyName.split('_')[-5:-3]) for keyName in self.keyNames if "g_"+corrType+"_vs_" in keyName and args.corrVar in keyName]
    etaRanges = list(set(etaRanges)) ## Remove duplicates (b/c of pt ranges) ##
    etaRanges = [etaRange for (etaLow,etaRange) in sorted(zip([int(etaRange.split('_')[0]) for etaRange in etaRanges ] ,etaRanges))]
    print "Using eta ranges", etaRanges
    return etaRanges

  #---------------------------------------------------------------------------------
  ## Load TGraphs from an input file for a given value of ieta, and find their corresponding pt values from the names ##
  def loadGraphs(self, etaRange):
    if( self.debug ):
      print "loadGraphs for eta", etaRange

    self.graphs = []
    self.graphNames = []
    self.pTbincenters = []
    self.pTlowbinedges = []
    self.pThighbinedges = []

    etaRange = '_eta_'+etaRange

    ## Get inclusive histogram (g_response_eta_X_Y) to find pt-corrected bin centers! ##
    inputInclusiveGraph = self.inputFile.Get( "g_response"+etaRange)
    if not inputInclusiveGraph:
      print "Error, could not find inclusive fit histogram g_response"+etaRange
      exit(1)
    centralPtValues = list(inputInclusiveGraph.GetX())

    #For this eta bin, find all histograms of correction name (g_respCorrected_vs_x_eta_X_Y_pt_X_Y) //
    for inames in range( 0, len(self.keyNames) ):

      if( "g_"+corrType+"_vs_" in self.keyNames[inames] and etaRange in self.keyNames[inames] and args.corrVar in self.keyNames[inames]):

        ## Build a new graph ##
        inputGraph = self.inputFile.Get( self.keyNames[inames] )
        g = self.BuildCleanGraph( inputGraph )


        ## Skip if there is no graph, probably because there was a 0 response (no entries)
        if not g:
          continue

        ## Skip if addPtValues returns False, which means the pt is too low
        passedPtCut = self.addPtValues( self.keyNames[inames], centralPtValues )
        if not passedPtCut:
          continue;

        g.SetName("Graph"+etaRange+"_pt"+str(len(self.graphs)) )
        self.graphNames.append(self.keyNames[inames])
        self.graphs.append(g)

        ## If the first and last pt are the same, throw an error ##
        if(len(self.pTbincenters) > 1 and self.pTbincenters[0] == self.pTbincenters[-1]):
          print "Error, duplicate values in pt list of ", self.pTbincenters
          exit(1)


        ## Get the original x-axis edges ##
        if(self.xLow_original == None):
          print self.keyNames[inames], "and", self.keyNames[inames].replace('g_'+corrType,'h_response')
          originalHistName = self.keyNames[inames].replace('g_'+corrType,'h_response')
          originalHist = self.inputFile.Get( originalHistName )
          self.xLow_original = originalHist.GetXaxis().GetBinLowEdge(1)
          self.xHigh_original = originalHist.GetXaxis().GetBinUpEdge( originalHist.GetNbinsX()+1 )


    return self.graphs

  #---------------------------------------------------------------------------------
  ## Parse the pt edges from the TGraph name, and save them to the pt lists ##
  def addPtValues(self, graphName, centralPtValues):
    if(args.debug):
      print "addPtValues"
    subStr = graphName.split('_')
    lowpt = float(subStr[-2])
    highpt = float(subStr[-1])
    if(not args.corrVar == "Nseg" and (lowpt==0 or highpt==0) ):
      print "pT edge can't be zero, exiting"
      exit(1)

    ptCenter = [pt for pt in centralPtValues if pt > lowpt and pt < highpt]

    if len(ptCenter) != 1:
      print "Error, could not find pt center for graph", graphName
      exit(1)
    ptCenter = ptCenter[0]

    if ptCenter < self.pTmin:
      return False

    self.pTlowbinedges.append(lowpt)
    self.pThighbinedges.append(highpt)
    self.pTbincenters.append( ptCenter );

    return True

  #---------------------------------------------------------------------------------
  ## Get minimum and maximum x-range from a list of TGraphs ##
  def GetMinMax(self, graphs):

    imin = 999
    imax = -999
    for graph in graphs:
      xValues = graph.GetX()

      imin = min(imin, xValues[0])
      imax = max(imax, xValues[graph.GetN()-1])


    return imin, imax

#---------------------------------------------------------------------------------
############################## LinearSmoothing class ##############################
## This class provides a first smoothing of each response TGraph as a function of pt ##
class LinearSmoothing():

  def __init__(self, name, minX, maxX, kernelfraction=5):
    self.name = name
    self.logX = False
    self.logY = False
    self.fraction=kernelfraction
    self.minX=minX
    self.maxX=maxX
    self.kernel = 0

  #---------------------------------------------------------------------------------
  ## Get weight of point, the inverse of the y error squared ##
  def GetW(self, graph, i):
    if graph.GetN() <= i:
      print "Cannot access point", i, "as the graph only has", graph.GetN(), "is."
      exit(1)
    if (graph.GetEY()[i]==0):
       print "ERROR: A point has error = 0, x={:.2f} y={:.2f}".format(graph.GetX()[i],graph.GetY()[i])
       exit(1)
    return 1.0/(graph.GetEY()[i]**2)


  #---------------------------------------------------------------------------------
  ## Return a smoothed histogram from a graph ##
  ## This does a linear smoothing of respone on just the variable in question, i.e. Tile0 ##
  def MakeSmoothHisto(self, graph, Nbins=50, xmin = None, xmax = None):

    ### Set the edges using this TGraph ##
    ### This can lead to odd features in the subsequent 2D gaussian smoothing ##
    #g_x = graph.GetX()
    #if (xmin == None):
    #  xmin = min(g_x)-abs(0.02*max(g_x))
    #if (xmax == None):
    #  xmax = 1.05*max(g_x)

    ### Set the X-variable edges using the lowest pt TGraph ##
    ### This leads to a flat extrapolation beyond the border of the data points after the subsequent 2D gaussian smoothing ##
    if (xmin == None):
      xmin = self.minX-0.02*self.maxX
    if (xmax == None):
      xmax = self.maxX*1.05

    h = ROOT.TH1F( "LinHist_"+graph.GetName(), "LinHist_"+graph.GetName(), Nbins, xmin, xmax)
    h.SetStats(0)


    g_ey = graph.GetEY()
    errorSum = 0
    for i in range(0,graph.GetN()):
      errorSum += g_ey[i]

    if( errorSum<0.05):
      extentfraction = 10;
    else:
      extentfraction = self.fraction

    for i in range(1, h.GetNbinsX()+1):
      x = h.GetBinCenter(i);
      binCont, binErr = self.GetValueAndError(graph, x, extentfraction);
      h.SetBinContent(i, binCont)
      h.SetBinError(i, binErr);

    h.GetXaxis().SetTitle( graph.GetXaxis().GetTitle() )
    h.GetYaxis().SetTitle( graph.GetYaxis().GetTitle() )

    return h;

  #---------------------------------------------------------------------------------
  ## Calculate the smoothed bin content and error for one point of a TGraph ##
  def GetValueAndError(self, g, x, extentfraction):
    if (g.GetN() == 0):
      print "Empty graph"
      exit(1)
    if (g.GetN() == 1):
       return g.GetY()[0], g.GetEY()[0]

    g_x = g.GetX()
    g_y = g.GetY()
    g_ey = g.GetEY()
    xmin = g_x[0]
    xmax = g_x[ g.GetN()-1]
    Dx = xmax-xmin
    self.kernel = Dx/extentfraction

    if(x < xmin):
      return g.GetY()[0], g.GetEY()[0]
    elif(x > xmax):
      x = xmax


    if self.logX and xmin <= 0:
      print "Error, attempting to set x-axis to log scale with non-positive values. Exiting"
      exit(1)

    sumw, sumwy = 0, 0
    ## Loop over each pair of pt points in the TGraph
    ## Contributing a weighted value to the bin content based on the distance to the point and error
    for i in range(0, g.GetN() ):
      x1 = g_x[i]
      if self.logX:
        x1 = log(x1)
      y1 = g_y[i]
      w1 = self.GetW(g,i)


      for j in range(i+1, g.GetN() ):
        x2 = g.GetX()[j]
        if self.logX:
          x2 = log(x2)
        y2 = g_y[j]
        w2 = self.GetW(g,j)


        if (x1 == x2):
          print "Not yet implemented points with same x values"
          exit(1)

        ## Calculate weighted y value ##
        maxDx = max( abs(x1-x), abs(x2-x) )
        wGaus = ROOT.TMath.Gaus(maxDx/self.kernel)
        mu_x = (w1*x1+w2*x2)/(w1+w2)
        mu_y = (w1*y1+w2*y2)/(w1+w2)
        dmu_y = math.sqrt(1.0/(w1+w2))

        # option 1
        k = (y2-mu_y)/(x2-mu_x)
        V_y2 = g_ey[j]**2 - dmu_y**2
        dk = math.sqrt(V_y2)/(x2-mu_x)

        # option 2 - equivalent
        #double k=(y2-y1)/(x2-x1), dk=sqrt(pow(g.GetEY()[i],2)+pow(g.GetEY()[j],2))/(x2-x1);

        e = math.sqrt( (dk*(x-mu_x))**2 + dmu_y**2 )
        w = 1.0/e/e;
        y = k*(x-mu_x)+mu_y
        w *= wGaus;
        sumwy += w*y
        sumw += w

    return sumwy/sumw, math.sqrt(1.0/sumw)

#---------------------------------------------------------------------------------
## Gaussian smoothing tool of 2D histogram ##
class GaussianSmoothing():

  def __init__(self, widthx, widthy, NbinsX, NbinsY, maxdistance=20):
    self.logx = False
    self.logy = False
    self.useError = True
    self.width_x = widthx
    self.width_y = widthy
    self.nbins_x = NbinsX
    self.nbins_y = NbinsY
    self.d_max = maxdistance

  #---------------------------------------------------------------------------------
  ## Get kernel weight for this point from x and y distance##
  def GetKernelWeight(self, x, y, xi, yi, erri, kernelFactor=1):


    dx = (math.log(x)-math.log(xi))/(self.width_x*kernelFactor) if self.logx else (x-xi)/(self.width_x*kernelFactor)
    dy = (math.log(y)-math.log(yi))/(self.width_y*kernelFactor) if self.logy else (y-yi)/(self.width_y*kernelFactor)
    dr = math.sqrt(dx**2+dy**2)

    #wi = math.exp( -dy / 2 ) #quicker than ROOT.TMath.Gaus(dr)
    wi = math.exp( -dr**2 / 2 ) #quicker than ROOT.TMath.Gaus(dr)

    if (self.useError):
      wi *= 1.0/erri/erri

    return wi;

  #---------------------------------------------------------------------------------
  ## Get smoothed linear value of this TGraph point usingg all points in 2D space ##
  def GetSmoothedLinearValue(self, g, x, y):
    sumw, sumwz = 0, 0

    g_ez = g.GetEZ()
    g_z = g.GetZ()
    g_x = g.GetX()
    g_y = g.GetY()

    iAttempt = 1
    while sumw == 0:
      sumwz = 0
      for i in range(0, g.GetN() ):
        wi = self.GetKernelWeight(x, y, g_x[i], g_y[i], g_ez[i], iAttempt);
        sumw += wi
        sumwz += wi*g_z[i]


      iAttempt += 1

    ##If sumw is zero, we're too far away from other points.
    ##Set to -999 for now, and we'll set it later to the closest value in corrVar space
    #if(sumw == 0):
    #  return -999

    return sumwz/sumw

  #---------------------------------------------------------------------------------
  ## Return a smoothed TH2F from an input TGraph ##
  def SmoothPtJetPropertyGraph(self, g2d, hname, pTmin=None, pTmax=None, xMin=None, xMax=None):
    self.logx = True
    self.logy = False

    ### Get binning for smoothed 2d histogram ##

    if(pTmin == None):
      pTmin = g2d.GetXaxis().GetXmin();
    if(pTmax == None):
      pTmax = g2d.GetXaxis().GetXmax();
    if(xMax == None):
      xMax = g2d.GetYaxis().GetXmax();
      xMax = xMax+(xMax*0.05)
    if(xMin == None):
      xMin = g2d.GetYaxis().GetXmin();
      xMin = xMin-(xMax*0.05)

    ## Get list of log-spaced  pt bins ##
    nPt = self.nbins_x
    log_dpt = (math.log(pTmax)-math.log(pTmin))/nPt
    ptbins_list = [ math.exp( math.log(pTmin) + i*log_dpt) for i in range(0, nPt+1) ]
    ptbins = array.array('d', ptbins_list )

    ## Get list of regularly spaced x-var bins ##

    nY = self.nbins_y
    dx = (xMax-xMin)/nY
    xbins_list = [ (xMin + i*dx) for i in range(0, nY+1) ]
    xbins = array.array('d', xbins_list )


    ## Create 2d histogram with new binning ##
    h = ROOT.TH2F(hname, hname, len(ptbins)-1, ptbins, len(xbins)-1, xbins);
    h.SetXTitle( g2d.GetXaxis().GetTitle() )
    h.SetYTitle( g2d.GetYaxis().GetTitle() )

    ## Fill each bin of new histogram with smoothed value from 2D tgraph##
    for ix in range(1, h.GetNbinsY()+1):
      for ipt in range(1, h.GetNbinsX()+1):
        x =  h.GetYaxis().GetBinCenter(ix)
        pt = h.GetXaxis().GetBinCenter(ipt)
        R = self.GetSmoothedLinearValue(g2d,pt,x)
        #double R=GetSmoothedInterpolatedValue(g2d,pt,x);
        h.SetBinContent(ipt,ix,R)

#    ## If a point is too far away in, set it to the same value as the previous (or next) corrVar variable in the same pt slice #
#    for ipt in range(1, h.GetNbinsX()+1):
#      for ix in range(2, h.GetNbinsY()+1):
#        thisBinContent = h.GetBinContent(ipt, ix)
#        prevBinContent = h.GetBinContent(ipt, ix-1)
#        if thisBinContent == -999 and prevBinContent != -999:
#          h.SetBinContent(ipt, ix, prevBinContent )
#      for ix in range(h.GetNbinsY(), 1, -1):
#        thisBinContent = h.GetBinContent(ipt, ix)
#        nextBinContent = h.GetBinContent(ipt, ix+1)
#        if thisBinContent == -999 and nextBinContent != -999:
#          h.SetBinContent(ipt, ix, nextBinContent )


    return h



#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--debug", dest='debug', action='store_true', default=False, help="More verbose for debugging.")
  parser.add_argument("--input", dest='input', required=True, default="", help="Input file name.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--ptMin", dest='ptMin', type=int, default=20, help="Minimum pt")
  parser.add_argument("--correction", dest='corrVar', default="", required=True, help="Correction variable for the x-axis.  Example are Tile0 and nTrk.  Set to None to skip variable fitting.")
  parser.add_argument("--jetType", dest='jetType', default="AntiKt4EMTopo", help="Jet type, used only for naming the output histograms and axes.")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = os.path.dirname(args.input)
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  RunGSCSmoothing()
  print "Done GSC smoothing"
