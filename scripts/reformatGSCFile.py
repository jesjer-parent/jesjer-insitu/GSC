import argparse
import ROOT
import AtlasStyle
import os
import math

def reformatGSCFile():

  inFile = ROOT.TFile.Open(args.input, "READ")
  outFile = ROOT.TFile.Open("reformatted_"+args.input, "RECREATE")

  ## Reformat existing histograms ##
  keys = inFile.GetListOfKeys()
  for key in keys:
    hist = inFile.Get(key.GetName())
    if type(hist) == ROOT.TH3F:
      continue
    print key.GetName()
    histName = hist.GetName()
    outName = histName.replace('Nseg','PunchThrough').replace('Ntrk','nTrk').replace('Wtrk','trackWIDTH').replace('CF','chargedFraction')
    if 'PunchThrough' in outName:
      if 'eta_19' in outName:
        continue
      outName = outName.replace('eta_13','eta_1')
    hist.SetName(outName)
    hist.SetTitle(outName)
    hist.SetDirectory(outFile)

  outFile.Write()
#  ## Add fake Tile0 and EM3 LC histograms ##
#  for i in range(0,35):
#    name = "AntiKt4LCTopo_EM3_interpolation_resp_eta_"+str(i)
#    newHist = ROOT.TH2F(name, name, 1, 20, 7000, 1, -0.1, 1)
#    newHist.SetBinContent(1,1, 1.)
#    newHist.Write()
#  for i in range(0,17):
#    name = "AntiKt4LCTopo_Tile0_interpolation_resp_eta_"+str(i)
#    newHist = ROOT.TH2F(name, name, 1, 20, 7000, 1, -0.1, 1)
#    newHist.SetBinContent(1,1, 1.)
#    newHist.Write()

  outFile.Close()


#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--input", dest='input', required=True, default="", help="Input file name")
  args = parser.parse_args()


  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  reformatGSCFile()
  print "Done reformatting GSC file"
