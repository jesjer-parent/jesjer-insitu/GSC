import argparse
import ROOT
import AtlasStyle
import os, math, array

def deriveAFIIUncertainty():

  m_mirrorEta = False

  inputFiles = [
                 "Fitted_Inclusive_FullSim_"+args.jetType+".root",
                 "Fitted_Inclusive_AFII_"+args.jetType+".root"
                 ]
  corrTypes = ["FullSim", "AFII"]
  fileColors = [ROOT.kBlack, ROOT.kRed-3]
  markerStyle = [20, 24]

  inputFiles = [args.path +'/' + inputFile for inputFile in inputFiles]


  #3D file : responseType : eta
  respHists = []
  etaList = set([])

  ## Gather all input histograms from the various files ##
  for iF, inputFile in enumerate(inputFiles):
    inFile = ROOT.TFile.Open(inputFile, "READ")
    print(inputFile)
    keys = inFile.GetListOfKeys()
    respHists.append( {} )


    respHistNames = [key.GetName() for key in keys if "All_fitted_response" in key.GetName() ]

    for respHistName in respHistNames:
      etaString = '_'.join(respHistName.split('_')[-2:])
      etaList.add( etaString )

      hist = inFile.Get(respHistName)
      hist.SetDirectory(0)
      respHists[iF][etaString] = hist

  etaList = list(etaList)
  ## Get actual eta edges from etaList strings
  etaEdgeList_lowEdge = [float(etaRange.split('_')[0])/10.  for etaRange in etaList]
  etaEdgeList_highEdge = [float(etaRange.split('_')[1])/10.  for etaRange in etaList]
  etaEdgeList = list(set(etaEdgeList_lowEdge+etaEdgeList_highEdge)) #Get only unique edges
  etaEdgeList.sort()

  print("etaEdgeList is", etaEdgeList)

  if( m_mirrorEta ):
    etaCopy = etaEdgeList[:]
    etaCopy = etaCopy[1:] #Remove zero entry
    etaCopy.reverse()
    etaCopy = [-eta for eta in etaCopy] #Make negative
    mirroredEtaList = etaCopy+etaEdgeList
  else:
    mirroredEtaList = etaEdgeList

  # Get ptEdgeList using an example histogram (all hist will have same binning)
  ptEdgeList = []
  ptEdgeList.append( respHists[0][etaList[0]].GetXaxis().GetBinLowEdge(1) )
  for iBin in range(1, respHists[0][etaList[0]].GetNbinsX()+1):
    ptEdgeList.append( respHists[0][etaList[0]].GetXaxis().GetBinUpEdge(iBin) )

  print("ptEdgeList is", ptEdgeList)
  print("mirrored is ", mirroredEtaList)
  array_ptEdgeList = array.array('f', ptEdgeList)


  ####### Get relevant 1D histograms and add them to the 2D hist ########
  AFIIResponseDiff2D = ROOT.TH2D("RelativeNonClosure_AFII_"+args.jetType, "RelativeNonClosure_AFII_"+args.jetType, len(ptEdgeList)-1, array.array('f', ptEdgeList), len(mirroredEtaList)-1, array.array('f',mirroredEtaList) )

  numAbsEtaBins = len(etaList)-1

  for iEta, etaString in enumerate(etaList):
    lowEtaEdge = float(etaString.split('_')[0])/10.
    etaSpot = etaEdgeList.index(lowEtaEdge)
    print("etaSpot", etaSpot, "for string", etaString)

    Fullsim_response = respHists[0][etaString]
    AFII_response = respHists[1][etaString]

    AFIIResponseDiff = ROOT.TH1D( "AFIIResponseDiff_"+str(iEta), "AFIIResponseDiff_"+str(iEta), len(ptEdgeList)-1, array_ptEdgeList )

    ## Derive the response difference histograms between Fullsim and AFII
    for iBin in range(1, len(ptEdgeList) ):
      if( AFII_response.GetBinContent(iBin) > 0 and Fullsim_response.GetBinContent(iBin) > 0 ):
        AFIIResponseDiff.SetBinContent(iBin, AFII_response.GetBinContent(iBin) - Fullsim_response.GetBinContent(iBin) )
        #AFIIResponseDiff.SetBinContent(iBin, math.fabs(AFII_response.GetBinContent(iBin) - Fullsim_response.GetBinContent(iBin)) )
        AFIIResponseDiff.SetBinError(iBin, AFII_response.GetBinError(iBin)**2 + Fullsim_response.GetBinError(iBin)**2 )

    ### Freeze empty bins to be equal to last valid bin
    for iBin in range(1, len(ptEdgeList)-1 ):
      if AFIIResponseDiff.GetBinContent(iBin+1) == 0.:
        AFIIResponseDiff.SetBinContent(iBin+1, AFIIResponseDiff.GetBinContent(iBin) )
        AFIIResponseDiff.SetBinError(iBin+1, AFIIResponseDiff.GetBinError(iBin) )

    ### Smooth in pt the 1D histograms
    AFIIResponseDiff.Smooth(4)

    ## Stitch together the histograms in eta
    for iBinX in range(1, len(ptEdgeList) ):
      if( m_mirrorEta ):
        AFIIResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins+etaSpot+2, AFIIResponseDiff.GetBinContent(iBinX) )
        AFIIResponseDiff2D.SetBinContent( iBinX, numAbsEtaBins-etaSpot+1, AFIIResponseDiff.GetBinContent(iBinX) )
      else:
        AFIIResponseDiff2D.SetBinContent( iBinX, etaSpot+1, AFIIResponseDiff.GetBinContent(iBinX) )

  AFIIResponseDiff2D.Smooth()

  #Draw results
  c1 = ROOT.TCanvas()
  c1.SetLogx()
  c1.SetRightMargin(0.18)
  c1.SetLeftMargin(0.1)
  AFIIResponseDiff2D.GetZaxis().SetRangeUser(-0.032,0.032)

  AFIIResponseDiff2D.GetXaxis().SetTitle("p_{T}^{Reco}")
  AFIIResponseDiff2D.GetYaxis().SetTitle("#eta_{det}")
  AFIIResponseDiff2D.GetXaxis().SetTitleOffset(0.8) 
  AFIIResponseDiff2D.GetYaxis().SetTitleOffset(0.8) 
  AFIIResponseDiff2D.Draw("colz")
  c1.SaveAs(args.outDir+'/AFIIResponseDiff_'+args.jetType+'.png')
  c1.SaveAs(args.outDir+'/AFIIResponseDiff_'+args.jetType+'.eps')


  ## Save results to file
  outFile = ROOT.TFile.Open(args.outDir+'/AFIIUncertainties_'+args.jetType+'.root', "RECREATE")
  AFIIResponseDiff2D.Write()
  outFile.Close()

#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
  parser.add_argument("--path", dest='path', required=True, default="", help="Path of input files")
  parser.add_argument("--jetType", dest='jetType', required=True, default="AntiKt4EMTopo", help="Type of jet inputs, determining histogram names.  Like AntiKt4EMTopo.")
  args = parser.parse_args()

  if args.outDir == "":
    args.outDir = args.path+'/AFIIDifferenceResults/'
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  deriveAFIIUncertainty()
  print("Done deriveAFIIUncertainty")
